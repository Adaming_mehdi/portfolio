package com.fr.inti.dto;

import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class PortfolioCreateDTO {

	private boolean visible;

	@Positive
	private Long idUser;

}
