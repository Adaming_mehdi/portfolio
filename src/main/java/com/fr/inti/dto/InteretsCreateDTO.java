package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class InteretsCreateDTO {
	
	
		
	@NotBlank
	String description;
	
	@Positive
	Long idPortfolio;
	
	@Positive
	Long idPhoto;

}
