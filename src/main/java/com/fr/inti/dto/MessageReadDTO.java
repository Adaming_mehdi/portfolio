package com.fr.inti.dto;




import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;




import lombok.Data;

@Data
public class MessageReadDTO {


	@Positive
	private Long id;
	
	@NotBlank
	private String contentMessage;
	
	@Positive
	private Long idRecruter;
	
	@Positive
	private Long idUser;
	
	private boolean deleted;

	private String dateCreation;

	public void setUser(UserReadDTO dtoUser) {
		// TODO Auto-generated method stub
		
	}

	public void setRecruter(RecruterReadDTO dtoRecruter) {
		// TODO Auto-generated method stub
		
	}

	public CommentReadDTO getRecruter() {
		// TODO Auto-generated method stub
		return null;
	}

	public CommentReadDTO getUser() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
