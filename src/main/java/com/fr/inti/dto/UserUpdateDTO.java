package com.fr.inti.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
@Data
public class UserUpdateDTO {

    @Positive
    private Long id;
    @NotBlank
    private String nom;
    @NotBlank
    private String prenom;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    private String avatarUrl;

    @NotBlank
    private String personnalisation;

    private String dateCreation;
}
