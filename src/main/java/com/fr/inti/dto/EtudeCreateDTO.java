package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class EtudeCreateDTO {
	
	@NotBlank
	private String nom;

	private String description;

	private String dateDebut;

	private String dateFin;
		
	private Long domaineId;
	
	private Long organisationId;
	
	private Long portfolioId;
	
	private Long photoId;
	
}
