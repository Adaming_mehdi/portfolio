package com.fr.inti.dto;

import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class PortfolioReadDTO {

	@Positive
	private Long id;

	private boolean visible;

	private UserReadDTO user;

	private boolean deleted;

	private String dateCreation;

}
