package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class CommentCreateDTO {

	@NotBlank
	private String contentComment;

	@Positive
	private Long idRecruter;

	@Positive
	private Long idPortfolio;

}
