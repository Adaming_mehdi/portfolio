package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class CommentReadDTO {

	@Positive
	private Long id;

	@NotBlank
	private String contentComment;

	private RecruterReadDTO recruter;

	private PortfolioReadDTO portfolio;

	private boolean deleted;

	private String dateCreation;

}
