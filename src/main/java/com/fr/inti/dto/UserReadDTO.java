package com.fr.inti.dto;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserReadDTO {

    private Long id;

    private String nom;

    private String prenom;

    private String login;

    private String password;

    private String avatarUrl;

    private String personnalisation;

    private List<MessageReadDTO> messages = new ArrayList<>();

    private PortfolioReadDTO portFolio;

    private boolean deleted;

    private String dateCreation;

}
