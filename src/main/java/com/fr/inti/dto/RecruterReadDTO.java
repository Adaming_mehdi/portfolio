package com.fr.inti.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class RecruterReadDTO {

	@Positive
	private Long id;

	private String nom;

	private String prenom;

	@NotBlank
	private String login;

	@NotBlank
	private String password;

	private String avatarUrl;

	private List<MessageReadDTO> idmessages;

	private List<CommentReadDTO> idcomments;

	private FavorisReadDTO idfavoris;

	private boolean deleted;

	@PastOrPresent
	private String dateCreation;

}
