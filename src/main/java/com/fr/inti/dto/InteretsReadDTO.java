package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class InteretsReadDTO {
	
	
	@PastOrPresent
	String dateCrea;
	
	@Positive
	Long id;
	
	@NotBlank
	String description;
	
	@Positive
	Long idPhoto;

}
