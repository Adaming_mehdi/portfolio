package com.fr.inti.dto;


import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class PhotoCreateDTO {



	@NotBlank
	private String urlPhoto;

	private boolean deleted;



}
