package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class AdminUpdateDTO {
	
	@NotNull
	@Positive
	private Long id;
	
	@NotBlank
	private String nom;

	@NotBlank
	private String prenom;

	@NotBlank
	private String login;

	@NotBlank
	private String password;

	private String avatarUrl;
	
	private boolean deleted;

}
