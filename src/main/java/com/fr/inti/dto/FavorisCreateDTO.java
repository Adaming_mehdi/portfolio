package com.fr.inti.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FavorisCreateDTO {
	
	private long id;
	private String libelle;
	private List<DomaineDTO> domaines;
	private List<RecruterReadDTO> recruteurs;
	private boolean deleted;
	private LocalDateTime dateCreation;
	
}
