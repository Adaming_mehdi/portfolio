package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class OrganisationDTO {

	private Long id;

	@NotBlank
	private String nom;

	private String lienWeb;

	private String description;

}
