package com.fr.inti.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class AdminReadDTO {
	
	@NotEmpty
	@Positive
	private Long id;

	@NotBlank
	private String nom;

	@NotBlank
	private String prenom;

	@NotBlank
	private String login;

	private String avatarUrl;
	
	private boolean deleted;

	private String dateCreation;

}
