package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.*;

@Data
@Getter
@Setter
public class ProjectCreateDTO {
	
private String nom;
	
	@NotBlank
	private String description;
	
	@NotBlank
	private String dateDebut;
	
	@NotBlank
	private String dateFin;
	
	@Positive
	private Long domaine_id;
	
	@Positive
	private Long organisation_id;
	
	@Positive
	private Long portfolio_id;
	
	@Positive
	private Long photo_id;
	
	

}
