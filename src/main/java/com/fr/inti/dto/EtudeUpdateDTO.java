package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EtudeUpdateDTO {
	
	@NotNull
	private Long id;
	
	@NotBlank
	private String nom;

	private String description;

	private String dateDebut;

	private String dateFin;
	
	private Long domaineId;
	
	private Long organisationId;
	
	private Long portfolioId;
	
	private Long photoId;

}
