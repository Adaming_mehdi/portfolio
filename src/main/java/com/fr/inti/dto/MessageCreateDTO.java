package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class MessageCreateDTO {
	
	@NotBlank
	private String contentMessage;

	@Positive
	private Long idRecruter;
	
	@Positive
	private Long idUser;

}
