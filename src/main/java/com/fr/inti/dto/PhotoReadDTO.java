package com.fr.inti.dto;



import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class PhotoReadDTO {

	@Positive
	private Long id;

	@NotBlank
	private String urlPhoto;

	private boolean deleted;



}
