package com.fr.inti.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class RecruterCreateDTO {

	private String nom;

	private String prenom;

	@NotBlank
	private String login;

	@NotBlank
	private String password;

	private String avatarUrl;

	private List<MessageCreateDTO> messages;

	private List<CommentCreateDTO> comments;

	private FavorisCreateDTO favoris;

}
