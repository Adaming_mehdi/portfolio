package com.fr.inti.dto;



import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class AdminCreateDTO {

	@NotBlank
	private String nom;

	@NotBlank
	private String prenom;

	@NotBlank
	private String login;

	@NotBlank
	private String password;

	private String avatarUrl;
	

}
