package com.fr.inti.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class EtudeReadDTO {
	
	@NotBlank
	private String nom;

	private String description;

	private String dateDebut;

	private String dateFin;
	
	private boolean deleted;
	
	private Long id;
	
	private String photoUrl;
	
	private String nomOrganisation;
	
	private String nomDomaine;

}
