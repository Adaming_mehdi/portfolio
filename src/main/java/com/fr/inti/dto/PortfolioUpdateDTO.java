package com.fr.inti.dto;

import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class PortfolioUpdateDTO {

	@Positive
	private Long id;

	private boolean visible;

	@Positive
	private Long idUser;

}
