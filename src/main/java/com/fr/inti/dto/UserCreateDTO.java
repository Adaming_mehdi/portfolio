package com.fr.inti.dto;
import lombok.Data;
import javax.validation.constraints.NotBlank;

@Data
public class UserCreateDTO {

    @NotBlank
    private String nom;

    @NotBlank
    private String prenom;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    private String avatarUrl;

    @NotBlank
    private String personnalisation;

    private String dateCreation;

}