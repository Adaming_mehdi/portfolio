package com.fr.inti.repository;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Favoris;

@Repository
public interface FavorisRepository extends JpaRepository<Favoris, Long> {
	@Transactional
	@Modifying
	@Valid
	@Query(nativeQuery = true, value = "UPDATE Favoris SET deleted=true WHERE id=:id")
	public void fakeDelete(Long id);


}
