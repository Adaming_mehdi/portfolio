package com.fr.inti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Etude;

@Repository
public interface EtudeRepository extends JpaRepository<Etude, Long> {
	
	@Query(nativeQuery = true, value = "SELECT * FROM etude WHERE id_organisation = :idOrganisation")
	public List<Etude> findEtudeByOrganisation(@Param(value = "idOrganisation") Long idOrganisation);
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE etude SET deleted = true WHERE id = :id")
	public void updateDelete(Long id);

}
