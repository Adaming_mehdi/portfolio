package com.fr.inti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Photo;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
	@Transactional
	@Modifying
	@Query(value = "UPDATE Photo SET deleted=true WHERE id=:id", nativeQuery = true)
	public void fakeDelete(Long id);
}
