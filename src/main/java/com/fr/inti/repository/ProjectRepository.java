package com.fr.inti.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
	
	@Query(nativeQuery = true, value = "SELECT * FROM project WHERE domaine_id = :domaineId")
	public Project findProjectByDomaine(@Param(value = "domaineId") Long domaineId);
	
	@Query(nativeQuery = true, value = "SELECT * FROM project WHERE organisation_id = :organisationId")
	public Project findProjectByOrganisation(@Param(value = "organisationId") Long organisationId);
	
	@Query(nativeQuery = true, value = "SELECT * FROM project WHERE portfolio_id = :portfolioId")
	public Project findProjectByPortfolio(@Param(value = "portfolioId") Long portfolioId);
	
	@Query(nativeQuery = true, value = "SELECT * FROM project WHERE photo_id = :photoId")
	public Project findProjectByPhoto(@Param(value = "photoId") Long photoId);
	
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE project SET deleted = true WHERE id = :id")
	public void fakeDelete(Long id);

}
