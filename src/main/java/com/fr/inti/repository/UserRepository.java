package com.fr.inti.repository;
import com.fr.inti.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "UPDATE user SET deleted = true WHERE id = :id")
    public void updateDelete(Long id);

    @Query(nativeQuery = true, value = "SELECT * from user WHERE login = :login")
    public User findByLogin(String login);

    }
