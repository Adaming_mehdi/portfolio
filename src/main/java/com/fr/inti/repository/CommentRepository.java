package com.fr.inti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE Comment SET deleted=true WHERE id=:id")
	public void fakeDelete(Long id);
	
	public List<Comment> findByContentCommentContainingIgnoreCase(String contentComment);

}
