package com.fr.inti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Organisation;

@Repository
public interface OrganisationRepository extends JpaRepository<Organisation, Long> {

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE organisation SET deleted = true WHERE id = :id")
	public void updateDelete(Long id);

}
