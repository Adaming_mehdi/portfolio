package com.fr.inti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Portfolio;

@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE portfolio SET deleted=true WHERE id=:id")
	public void fakeDelete(Long id);

	public List<Portfolio> findByVisibleTrue();
	
}
