package com.fr.inti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.inti.entity.Domaine;

@Repository
public interface DomaineRepository extends JpaRepository<Domaine, Long> {
	
	@Transactional
    @Modifying
	@Query(nativeQuery = true, value = "UPDATE domaine SET deleted=true WHERE id=:id")
	public void fakeDelete(Long id);


}
