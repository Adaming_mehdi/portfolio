package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.fr.inti.entity.Recruter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.RecruterConverter;
import com.fr.inti.dto.RecruterCreateDTO;
import com.fr.inti.dto.RecruterReadDTO;
import com.fr.inti.repository.RecruterRepository;

@RestController
@RequestMapping(path = "/recruter")
public class RecruterController {

	@Autowired
	RecruterRepository recruterDao;

	// READ-ALL
	@GetMapping(path = "/all")
	public List<RecruterReadDTO> findAll() {
		return RecruterConverter.convertToDtoList(recruterDao.findAll());
	}

	// READ-BY-ID
	@GetMapping(path = "/find/{id}")
	public RecruterReadDTO findById(@PathVariable Long id) {
		if (id == null) {
			return null;
		}
		Optional<Recruter> recruter = recruterDao.findById(id);

		if (recruter.isPresent()) {
			return RecruterConverter.convertToDTO(recruter.get());
		}
		return null;
	}

	// CREATE OR UPDATE
	@PostMapping
	public String createRecruter(@Valid @RequestBody RecruterCreateDTO recruter) {
		if (recruter == null) {
			return null;
		}
		recruterDao.save(RecruterConverter.convertCreateDTOToEntity(recruter));
		return "Recruteur crée / modifié avec succès";
	}

	// DELETE
	@DeleteMapping(path = "/delete/{id}")
	public String deleteRecruterById(@PathVariable Long id) {
		if (id == null) {
			return null;
		}
		recruterDao.deleteById(id);
		return "Recruteur supprimé avec succès";
	}

}
