package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.AdminConverter;
import com.fr.inti.dto.AdminCreateDTO;
import com.fr.inti.dto.AdminReadDTO;
import com.fr.inti.dto.AdminUpdateDTO;
import com.fr.inti.entity.Admin;
import com.fr.inti.repository.AdminRepository;

@RestController
@RequestMapping(path = "/admin")
class AdminController {

	@Autowired
	AdminRepository dao;

	// READ-ALL
	@GetMapping
	public List<AdminReadDTO> findAll() {
		return AdminConverter.convertToReadDtoList(dao.findAll());
	}
	
	// READ-BY-ID
	@GetMapping(path= "/{id}")
	public AdminReadDTO findById(@PathVariable Long id) {
		
		Optional<Admin> adm = dao.findById(id);
		
		if (adm.isPresent()) {
			
			return AdminConverter.convertToReadDTO(adm.get());
		}
		
		return null;
	}

	// CREATE
	@PostMapping
	public String create(@Valid @RequestBody AdminCreateDTO admin) {
		dao.save(AdminConverter.convertToCreateEntity(admin));
		return "Administrateur créé";
	}
	
	// UPDATE
	@PutMapping
	public String update(@Valid @RequestBody AdminUpdateDTO admin) {
		dao.save(AdminConverter.convertToUpdateEntity(admin));
		return "Administrateur modifié";
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable Long id) {
		dao.fakeDelete(id);
		return "Administrateur supprimé";
	}

}
