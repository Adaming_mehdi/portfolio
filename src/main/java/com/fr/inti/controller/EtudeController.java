package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.EtudeConverter;
import com.fr.inti.dto.EtudeCreateDTO;
import com.fr.inti.dto.EtudeReadDTO;
import com.fr.inti.dto.EtudeUpdateDTO;
import com.fr.inti.entity.Etude;
import com.fr.inti.repository.EtudeRepository;

@RestController
@RequestMapping(path = "/etude")
public class EtudeController {

	@Autowired
	EtudeRepository etudeDao;

	// READ-ALL
	@GetMapping
	public List<EtudeReadDTO> findAll() {
		return EtudeConverter.convertToDtoList(etudeDao.findAll());
	}
	
	@GetMapping(path = "/{id}")
	public EtudeReadDTO findById(@PathVariable Long id) {
		EtudeReadDTO dto = new EtudeReadDTO();
		Optional<Etude> etude = etudeDao.findById(id);
		if (etude.isPresent()) {
			dto = EtudeConverter.convertToDto(etude.get());
		}
		return dto;
	}

	// CREATE
	@PostMapping
	public void createEtude(@Valid @RequestBody EtudeCreateDTO etudeDto) {
		if (etudeDto != null) {
			etudeDao.save(EtudeConverter.convertToEntity(etudeDto));
		}
	}
	
	@PutMapping
	public void updateEtude(@Valid @RequestBody EtudeUpdateDTO etudeDto) {
		if (etudeDto != null && etudeDto.getId() != null && etudeDao.existsById(etudeDto.getId())) {
			Optional<Etude> etude = etudeDao.findById(etudeDto.getId());
			if (etude.isPresent()) {
				etudeDao.save(EtudeConverter.convertToEntity(etudeDto, etude.get()));
			}
		}
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public void deleteEtudeById(@PathVariable Long id) {
		etudeDao.updateDelete(id);
	}

}
