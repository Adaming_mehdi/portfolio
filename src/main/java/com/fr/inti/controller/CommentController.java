package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.CommentConverter;
import com.fr.inti.dto.CommentCreateDTO;
import com.fr.inti.dto.CommentReadDTO;
import com.fr.inti.dto.CommentUpdateDTO;
import com.fr.inti.entity.Comment;
import com.fr.inti.repository.CommentRepository;

@RestController
@RequestMapping(path = "/comment")
public class CommentController {

	@Autowired
	CommentRepository dao;

// READ BY ID

	@GetMapping(path = "/{id}")
	public CommentReadDTO getById(@NotNull @Positive @PathVariable Long id) {
		Optional<Comment> oC = dao.findById(id);
		if (oC.isPresent()) {
			return CommentConverter.convertToReadDTO(oC.get());
		}
		return null;
	}

// READ ALL

	@GetMapping
	public List<CommentReadDTO> findAll() {
		return CommentConverter.convertToDtoList(dao.findAll());
	}

// READ ALL CONTENT CONTAINING STRING INJECTED

	@GetMapping(path = "/content/{contentComment}")
	public List<CommentReadDTO> findComment(@NotBlank @PathVariable String contentComment) {
		return CommentConverter.convertToDtoList(dao.findByContentCommentContainingIgnoreCase(contentComment));
	}

// CREATE

	@PostMapping
	public String create(@Valid @RequestBody CommentCreateDTO commentDTO) {
		dao.save(CommentConverter.convertToEntity(commentDTO));
		return "Comment submitted";
	}

// UPDATE

	@PutMapping
	public String update(@Valid @RequestBody CommentUpdateDTO commentDTO) {
		dao.save(CommentConverter.convertToEntity(commentDTO));
		return "Comment updated";
	}

// DELETE

	@DeleteMapping(path = "/{id}")
	public String deleteById(@NotNull @Positive @PathVariable Long id) {
		dao.fakeDelete(id);
		return "Comment deleted";
	}

}
