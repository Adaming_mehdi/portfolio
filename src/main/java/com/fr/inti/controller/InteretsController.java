package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.InteretsConverter;
import com.fr.inti.dto.InteretsCreateDTO;
import com.fr.inti.dto.InteretsReadDTO;
import com.fr.inti.entity.Interets;
import com.fr.inti.repository.InteretsRepository;


@RestController
@RequestMapping(path = "/Interets")
public class InteretsController {

	@Autowired
	InteretsRepository dao;
	

	// READ-ALL
	@GetMapping
	public List<InteretsReadDTO> findAll() {
		return InteretsConverter.convertToListDTO(dao.findAll());
	}
	
	@GetMapping(path = "/{id}")
	public InteretsReadDTO findById(@PathVariable Long id) {
		InteretsReadDTO dto = new InteretsReadDTO();
		Optional<Interets> interets = dao.findById(id);
		if (interets.isPresent()) {
			dto = InteretsConverter.convertToDTO(interets.get());
		}
		return dto;
	}
	

	// CREATE
	
	@PostMapping
	public String create(@Valid @RequestBody InteretsCreateDTO interet) {
		dao.save(InteretsConverter.convertToEntity(interet));
		return "Crée";
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable long id) {
		Optional<Interets> interets = dao.findById(id);
		if(interets.isPresent()) {
			dao.fakeDelete(id);
		}
		return "Supprimé";
	}

}
