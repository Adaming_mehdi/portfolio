package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.fr.inti.converter.ProjectConverter;

import com.fr.inti.dto.ProjectCreateDTO;
import com.fr.inti.dto.ProjectReadDTO;
import com.fr.inti.entity.Project;
import com.fr.inti.repository.ProjectRepository;

@RestController
@RequestMapping(path = "/project")
public class ProjectController {

	@Autowired
	ProjectRepository projectDao;
	
	// GET BY ID
	@GetMapping(path= "/{id}")
	public ProjectReadDTO findById(@Valid @PathVariable Long id) {
		Optional<Project> project = projectDao.findById(id);
		if (project.isPresent()) {
			
			return ProjectConverter.convertToReadDTO(project.get());
		}
		
		return null;
	}

	// READ-ALL
	@GetMapping
	public List<ProjectReadDTO> findAll() {
		return ProjectConverter.convertToDtoList(projectDao.findAll());
	}

	// CREATE
	@PostMapping
	public String create(@RequestBody ProjectCreateDTO projectCreateDto)  {
		projectDao.save(ProjectConverter.convertToCreateEntity(projectCreateDto));
		return "Le projet a été crée";
	}
	// UPDATE
	@PutMapping
	public String update(@Valid @RequestBody ProjectReadDTO projectReadDto) {
		if (projectDao.existsById(projectReadDto.getId())) {
			projectDao.save(ProjectConverter.convertToReadEntity(projectReadDto));
			return "Projet mis à jour";
		}
		else {
			return "Projet non trouvé";
		}

	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable Long id) {
		if (projectDao.existsById(id)) {
			projectDao.deleteById(id);
			return "SUCCESS";
		}else {return "Projet non trouvé";}
		
	}

}
