package com.fr.inti.controller;
import java.util.List;
import java.util.Optional;
import com.fr.inti.converter.UserConverter;
import com.fr.inti.dto.UserCreateDTO;
import com.fr.inti.dto.UserReadDTO;
import com.fr.inti.dto.UserUpdateDTO;
import com.fr.inti.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fr.inti.repository.UserRepository;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/user")
public class UserController {

	@Autowired
	UserRepository dao;


	@GetMapping(path = "/{id}")
	public UserReadDTO findById(@PathVariable Long id) {
		Optional<User> user = dao.findById(id);
		if (user.isPresent()) {
			return UserConverter.convertToReadDTO(user.get());
		}
		return null;
	}


	@GetMapping
	public List<UserReadDTO> findAll() {
		return UserConverter.convertToDtoList(dao.findAll());
	}


	@PostMapping
	public String create(@Valid @RequestBody UserCreateDTO userDTO) {
		String response;
			if(dao.findByLogin(userDTO.getLogin()) == null){
				dao.save(UserConverter.convertToCreateEntity(userDTO));
				response = " l'utilisateur : "+ userDTO.getNom() +" "+ userDTO.getPrenom() + " a été créé";
			} else {
				response = "Le Login existe déjà, veuillez en choisir un autre";

			}
		return response;
	}

	@PostMapping(path = "/update")
	public String updateByID(@Valid @RequestBody UserUpdateDTO userUpdateDTO) {
		Optional<User> user = dao.findById(userUpdateDTO.getId());
		if(user.isPresent()) {
			dao.save(UserConverter.convertToUpdateEntity(userUpdateDTO));
			return "L'utilisateur : "+userUpdateDTO.getNom()+" "+userUpdateDTO.getPrenom() +  " a été modifié";
		} else {
			return " L'utilisateur n'existe pas";
		}
	}


	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable Long id) {
		Optional<User> user = dao.findById(id);
		if (user.isPresent()) {
		   if (!user.get().isDeleted())	{
			   dao.updateDelete(id);
			   return "L'utilisateur : " + user.get().getNom() + " " + user.get().getPrenom() + " a été supprimé";
		   }
		   return "l'utilisateur est déjà supprimé";
		} else {
			return " L'utilisateur n'existe pas";
		}
	}
}
