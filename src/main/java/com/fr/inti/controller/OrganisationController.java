package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.OrganisationConverter;
import com.fr.inti.dto.OrganisationDTO;
import com.fr.inti.entity.Organisation;
import com.fr.inti.repository.OrganisationRepository;

@RestController
@RequestMapping(path = "/organisation")
public class OrganisationController {

	@Autowired
	OrganisationRepository organisationDao;

	// READ-ALL
	@GetMapping
	public List<OrganisationDTO> findAll() {
		return OrganisationConverter.convertToDtoList(organisationDao.findAll());
	}

	@GetMapping(path = "/{id}")
	public OrganisationDTO findById(@PathVariable Long id) {
		OrganisationDTO dto = new OrganisationDTO();
		Optional<Organisation> organisation = organisationDao.findById(id);
		if (organisation.isPresent()) {
			dto = OrganisationConverter.convertToDto(organisation.get());
		}
		return dto;
	}

	// CREATE
	@PostMapping
	public void createOrganisation(@Valid @RequestBody OrganisationDTO organisationDto) {
		organisationDao.save(OrganisationConverter.convertToEntity(organisationDto));
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public void deleteOrganisationById(@PathVariable Long id) {
		organisationDao.updateDelete(id);
	}

}
