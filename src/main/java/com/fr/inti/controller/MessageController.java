package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.fr.inti.converter.MessageConverter;
import com.fr.inti.dto.MessageReadDTO;
import com.fr.inti.entity.Message;
import com.fr.inti.repository.MessageRepository;
import com.fr.inti.dto.MessageUpdateDTO;

@RestController
@RequestMapping(path = "/message")
public class MessageController {

	@Autowired
	MessageRepository dao;

	// READ BY ID

	@GetMapping(path = "/{id}")
	public MessageReadDTO getById(@NotNull @PathVariable Long id) {
		Optional<Message> msg = dao.findById(id);
		Message message = new Message();
		if (msg.isPresent()) {
			message = msg.get();
		}

		return MessageConverter.convertToReadDTO(message);
	}
	
	// READ-ALL
	@GetMapping
	public List<MessageReadDTO> findAll() {
		return MessageConverter.convertToDtoList(dao.findAll());
	}

	// CREATE
	@PostMapping
	public String create(@Valid @RequestBody MessageReadDTO MessageDTO) {
		dao.save(MessageConverter.convertToEntity(MessageDTO));
		return "Message submitted";
	}

	// UPDATE

	@PutMapping
	public String update(@Valid @RequestBody MessageUpdateDTO messageDTO) {
			dao.save(MessageConverter.convertToEntity(messageDTO));
			return "Message updated";
	}

	
	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@NotNull @PathVariable long id) {
		dao.fakeDelete(id);
		return "Message deleted";
	}
}
