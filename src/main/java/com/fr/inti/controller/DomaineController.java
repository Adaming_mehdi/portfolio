package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.DomaineConverter;
import com.fr.inti.dto.DomaineDTO;
import com.fr.inti.entity.Domaine;
import com.fr.inti.repository.DomaineRepository;

@RestController
@RequestMapping(path = "/domaine")
public class DomaineController {

	@Autowired
	DomaineRepository dao;

	// READ
	@GetMapping(path = "/{id}")
	public DomaineDTO findById(@PathVariable Long id) {
		DomaineDTO dto = new DomaineDTO();
		Optional<Domaine> domaine = dao.findById(id);
		if (domaine.isPresent()) {
			dto = DomaineConverter.convertToDTO(domaine.get());
		}
		return dto;
	}

	// READ-ALL
	@GetMapping
	public List<DomaineDTO> findAll() {
		return DomaineConverter.convertToDtoList(dao.findAll());
	}

	// CREATE
	@PostMapping
	public String create(@RequestBody DomaineDTO domaineDTO) {
		dao.save(DomaineConverter.convertToEntity(domaineDTO));
		return "Le domaine a été crée";
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable Long id) {
		dao.fakeDelete(id);
		return "Domaine deleted";
	}

}
