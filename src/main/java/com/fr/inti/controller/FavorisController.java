package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.FavorisConverter;
import com.fr.inti.dto.FavorisCreateDTO;
import com.fr.inti.dto.FavorisReadDTO;
import com.fr.inti.entity.Favoris;
import com.fr.inti.repository.FavorisRepository;

@RestController
@RequestMapping(path = "/favoris")
public class FavorisController {
	

	@Autowired
	FavorisRepository dao;

	// READ-ALL
	@GetMapping(path= "/all")
	public List<FavorisCreateDTO> findAll() {
		return FavorisConverter.convertToDtoList(dao.findAll());
	}

	// CREATE
	
	@PostMapping
	public String create(@Valid @RequestBody FavorisCreateDTO favorisDTO) {
		dao.save(FavorisConverter.convertToEntity(favorisDTO));
		return "Le Favoris a été crée";
	}

	// DELETE
	
		@DeleteMapping(path = "/del/{id}")
		public String deleteById ( @Valid @PathVariable Long id) {
			Optional<Favoris> favoris = dao.findById(id);
			if(favoris.isPresent()) {
			dao.fakeDelete(id);
			return "Favoris supprimé";
		}else {
			return "Favoris introuvable";
		}
		}
	
//READ-BY-ID
	
	@GetMapping(path= "/find/{id}")
	public FavorisReadDTO findById(@Valid @PathVariable Long id) {
		Optional<Favoris> favoris = dao.findById(id);
		if (favoris.isPresent()) {
			
			return FavorisConverter.convertToDTOReadOnly(favoris.get());
		}
		
		return null;
	}
		 
	// UPDATE
	
	@PutMapping
	public void update(@Valid @RequestBody FavorisCreateDTO favorisDto) {
		if (favorisDto != null && dao.existsById(favorisDto.getId())) {
			dao.save(FavorisConverter.convertToEntity(favorisDto));
		}
	}

		
		 
	}
