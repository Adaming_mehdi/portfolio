package com.fr.inti.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.PortfolioConverter;
import com.fr.inti.dto.PortfolioCreateDTO;
import com.fr.inti.dto.PortfolioReadDTO;
import com.fr.inti.dto.PortfolioUpdateDTO;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.repository.PortfolioRepository;

@RestController
@RequestMapping(path = "/portfolio")
public class PortfolioController {

	@Autowired
	PortfolioRepository dao;

// READ BY ID

	@GetMapping(path = "/{id}")
	public PortfolioReadDTO getById(@NotNull @Positive @PathVariable Long id) {
		Optional<Portfolio> oP = dao.findById(id);
		if (oP.isPresent()) {
			return PortfolioConverter.convertToReadDTO(oP.get());
		}
		return null;
	}

// READ ALL

	@GetMapping
	public List<PortfolioReadDTO> findAll() {
		return PortfolioConverter.convertToDtoList(dao.findAll());
	}

// READ ALL VISIBLE TRUE

	@GetMapping(path = "/visible")
	public List<PortfolioReadDTO> findVisible() {
		return PortfolioConverter.convertToDtoList(dao.findByVisibleTrue());
	}

// CREATE

	@PostMapping
	public String create(@Valid @RequestBody PortfolioCreateDTO portfolioDTO) {
		dao.save(PortfolioConverter.convertToEntity(portfolioDTO));
		return "Portfolio created";
	}

// UPDATE

	@PutMapping
	public String update(@Valid @RequestBody PortfolioUpdateDTO portfolioDTO) {
		dao.save(PortfolioConverter.convertToEntity(portfolioDTO));
		return "Portfolio updated";
	}

// DELETE

	@DeleteMapping(path = "/{id}")
	public String deleteById(@NotNull @Positive @PathVariable Long id) {
		dao.fakeDelete(id);
		return "Comment deleted";
	}

}
