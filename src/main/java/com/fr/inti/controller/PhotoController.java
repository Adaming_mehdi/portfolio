package com.fr.inti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.inti.converter.PhotoConverter;
import com.fr.inti.dto.PhotoCreateDTO;
import com.fr.inti.repository.PhotoRepository;

@RestController
@RequestMapping(path = "/photo")
public class PhotoController {

	@Autowired
	PhotoRepository dao;

	// READ-ALL
	@GetMapping
	public List<PhotoCreateDTO> findAll() {
		return PhotoConverter.convertToDtoList(dao.findAll());
	}

	// CREATE
	@PostMapping
	public String create(@RequestBody PhotoCreateDTO photoDTO) {
		dao.save(PhotoConverter.convertToEntity(photoDTO));
		return "Photo added";
	}

	// DELETE
	@DeleteMapping(path = "/{id}")
	public String deleteById(@PathVariable Long id) {
		dao.deleteById(id);
		return "Photo removed";
	}

}
