package com.fr.inti.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "etude")
public class Etude extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String nom;

	@Column
	private String description;

	@Column
	private LocalDate dateDebut;

	@Column
	private LocalDate dateFin;

	@ManyToOne
	@JoinColumn(name = "id_organisation")
	private Organisation organisation;

	@ManyToOne
	@JoinColumn(name = "id_domaine")
	private Domaine domaine;

	@ManyToOne
	@JoinColumn(name = "id_portfolio")
	private Portfolio portfolio;

	@ManyToOne
	@JoinColumn(name = "id_photo")
	private Photo photo;

}
