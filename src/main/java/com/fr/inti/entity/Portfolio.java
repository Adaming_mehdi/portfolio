package com.fr.inti.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "portfolio")
public class Portfolio extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private Boolean visible;

	@OneToOne(mappedBy = "portfolio")
	private User user;

	@OneToMany(mappedBy = "portfolio")
	private List<Comment> comments;

	@OneToMany(mappedBy = "portfolio")
	private List<Project> projects;

	@OneToMany(mappedBy = "portfolio")
	private List<Interets> interets;

	@OneToMany(mappedBy = "portfolio")
	private List<Etude> etudes;

}
