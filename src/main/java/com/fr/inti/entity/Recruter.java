package com.fr.inti.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "recruter")
public class Recruter extends Personne {

	@ManyToOne
	@JoinColumn(name = "id_favoris")
	private Favoris favoris;

	@OneToMany(mappedBy = "recruter")
	private List<Comment> comments;

	@OneToMany(mappedBy = "recruter")
	private List<Message> messages;

}
