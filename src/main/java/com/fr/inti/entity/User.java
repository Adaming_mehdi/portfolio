package com.fr.inti.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "user")
public class User extends Personne {

	@Column
	private String personnalisation;

	@OneToMany(mappedBy = "user")
	private List<Message> messages = new ArrayList<>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_Portfolio")
	private Portfolio portfolio;



	public User(Long id, String nom, String prenom, String login, String password, String avatarUrl, String personnalisation) {
		super(id, nom, prenom, login, password, avatarUrl);
		this.personnalisation = personnalisation;
	}
}
