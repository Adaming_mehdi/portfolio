package com.fr.inti.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "photo")
public class Photo extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String urlPhoto;

	@OneToMany(mappedBy = "photo")
	private List<Etude> etudes;

	@OneToMany(mappedBy = "photo")
	private List<Project> projects;

	@OneToMany(mappedBy = "photo")
	private List<Interets> interets;

}
