package com.fr.inti.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public abstract class AbstractModel {

	@Column(columnDefinition = "boolean default false", updatable = false)
	private boolean deleted = false;

	@Column(updatable = false)
	@CreationTimestamp
	private LocalDateTime dateCreation;

}
