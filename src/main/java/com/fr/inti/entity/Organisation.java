package com.fr.inti.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "organisation")
public class Organisation extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String nom;

	@Column
	private String lienWeb;

	@Column
	private String description;

	@OneToMany(mappedBy = "organisation")
	private List<Etude> etudes;

	@OneToMany(mappedBy = "organisation")
	private List<Project> projects;

	public Organisation(Long id) {
		super();
		this.id = id;
	}

}
