package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;

import com.fr.inti.dto.DomaineDTO;
import com.fr.inti.entity.Domaine;

public class DomaineConverter {

	private DomaineConverter() {
	}

	// create and read
	public static Domaine convertToEntity(DomaineDTO dto) {
		if (dto == null) {
			return null;
		}

		Domaine domaine = new Domaine();
		domaine.setLibelle(dto.getLibelle());

		return domaine;
	}

	public static DomaineDTO convertToDTO(Domaine domaine) {
		if (domaine == null) {
			return null;
		}
		DomaineDTO dto = new DomaineDTO();
		dto.setLibelle(domaine.getLibelle());

		return dto;
	}

	// READ ALL

	public static List<DomaineDTO> convertToDtoList(List<Domaine> entityList) {
		if (Objects.isNull(entityList)) {
			return Collections.emptyList();
		}
		List<DomaineDTO> dtoList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(entityList)) {
			for (Domaine entity : entityList) {
				dtoList.add(convertToDTO(entity));
			}
		}
		return dtoList;
	}

	public static List<Domaine> convertToEntityList(List<DomaineDTO> dtoList) {
		if (Objects.isNull(dtoList)) {
			return Collections.emptyList();
		}
		List<Domaine> entityList = new ArrayList<>();
		for (DomaineDTO domaineDTO : dtoList) {
			entityList.add(convertToEntity(domaineDTO));
		}
		return entityList;
	}

}