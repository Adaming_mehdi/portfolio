package com.fr.inti.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fr.inti.dto.PortfolioCreateDTO;
import com.fr.inti.dto.PortfolioReadDTO;
import com.fr.inti.dto.PortfolioUpdateDTO;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.entity.User;

public class PortfolioConverter {

// READ CONVERTER

	private PortfolioConverter() {
	}

	public static Portfolio convertToEntity(PortfolioReadDTO dto) {
		if (dto == null)
			return null;

		Portfolio portfolio = new Portfolio();

		portfolio.setId(dto.getId());
		portfolio.setVisible(dto.isVisible());
		if (dto.getUser() != null) {
			User user = new User();
			user.setId(dto.getUser().getId());
			portfolio.setUser(user);
		}
		portfolio.setDeleted(dto.isDeleted());
		portfolio.setDateCreation(LocalDateTime.parse(dto.getDateCreation()));

		return portfolio;
	}

	public static PortfolioReadDTO convertToReadDTO(Portfolio portfolio) {
		if (portfolio == null)
			return null;

		PortfolioReadDTO dto = new PortfolioReadDTO();

		dto.setId(portfolio.getId());
		dto.setVisible(portfolio.getVisible());
		if (portfolio.getUser() != null)
			dto.setUser(UserConverter.convertToReadDTO(portfolio.getUser()));
		dto.setDeleted(portfolio.isDeleted());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("'Créé le' yyyy-MM-dd 'à' HH:mm:ss");
		dto.setDateCreation(portfolio.getDateCreation().format(formatter));

		return dto;
	}

// READ ALL CONVERTER

	public static List<Portfolio> convertToEntityList(List<PortfolioReadDTO> dtoList) {
		if (dtoList == null)
			return Collections.emptyList();

		List<Portfolio> entityList = new ArrayList<>();
		for (PortfolioReadDTO portfolioDTO : dtoList) {
			entityList.add(convertToEntity(portfolioDTO));
		}

		return entityList;
	}

	public static List<PortfolioReadDTO> convertToDtoList(List<Portfolio> portfolioList) {
		if (portfolioList == null)
			return Collections.emptyList();

		List<PortfolioReadDTO> dtoList = new ArrayList<>();
		for (Portfolio portfolio : portfolioList) {
			dtoList.add(convertToReadDTO(portfolio));
		}

		return dtoList;
	}

// CREATE CONVERTER

	public static Portfolio convertToEntity(PortfolioCreateDTO dto) {
		if (dto == null)
			return null;

		Portfolio portfolio = new Portfolio();

		portfolio.setVisible(dto.isVisible());
		if (dto.getIdUser() != null) {
			User user = new User();
			user.setId(dto.getIdUser());
			portfolio.setUser(user);
		}

		return portfolio;
	}

	public static PortfolioCreateDTO convertToCreateDTO(Portfolio portfolio) {
		if (portfolio == null)
			return null;

		PortfolioCreateDTO dto = new PortfolioCreateDTO();

		dto.setVisible(portfolio.getVisible());
		dto.setIdUser(portfolio.getUser().getId());

		return dto;
	}

// UPDATE CONVERTER

	public static Portfolio convertToEntity(PortfolioUpdateDTO dto) {
		if (dto == null)
			return null;

		Portfolio portfolio = new Portfolio();

		portfolio.setId(dto.getId());
		portfolio.setVisible(dto.isVisible());
		if (dto.getIdUser() != null) {
			User user = new User();
			user.setId(dto.getIdUser());
			portfolio.setUser(user);
		}

		return portfolio;
	}

	public static PortfolioUpdateDTO convertToUpdateDTO(Portfolio portfolio) {
		if (portfolio == null)
			return null;

		PortfolioUpdateDTO dto = new PortfolioUpdateDTO();

		dto.setId(portfolio.getId());
		dto.setVisible(portfolio.getVisible());
		dto.setIdUser(portfolio.getUser().getId());

		return dto;
	}

}
