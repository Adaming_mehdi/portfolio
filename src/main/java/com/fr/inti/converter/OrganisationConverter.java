package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.fr.inti.dto.OrganisationDTO;
import com.fr.inti.entity.Organisation;

public class OrganisationConverter {

	private OrganisationConverter() {
	}

	public static Organisation convertToEntity(OrganisationDTO dto) {
		if (dto != null) {
			Organisation entity = new Organisation();

			entity.setNom(dto.getNom());
			entity.setLienWeb(dto.getLienWeb());
			entity.setDescription(dto.getDescription());

			return entity;
		}
		return null;
	}

	public static OrganisationDTO convertToDto(Organisation entity) {
		if (entity != null) {
			OrganisationDTO dto = new OrganisationDTO();

			dto.setId(entity.getId());
			dto.setNom(entity.getNom());
			dto.setLienWeb(entity.getLienWeb());
			dto.setDescription(entity.getDescription());

			return dto;
		}
		return null;
	}

	public static List<OrganisationDTO> convertToDtoList(List<Organisation> entityList) {
		if (entityList == null)
			return Collections.emptyList();

		List<OrganisationDTO> dtoList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(entityList)) {
			for (Organisation entity : entityList) {
				dtoList.add(convertToDto(entity));
			}
		}
		return dtoList;
	}

}
