package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.fr.inti.dto.PhotoCreateDTO;
import com.fr.inti.dto.PhotoReadDTO;
import com.fr.inti.entity.Photo;

public class PhotoConverter {

	public static Photo convertToEntity(PhotoCreateDTO photoDTO) {
		Photo photo = new Photo();

		photo.setUrlPhoto(photoDTO.getUrlPhoto());
		photo.setDeleted(photoDTO.isDeleted());
		// photo.setDateCreation(LocalDateTime.parse(dto.getDateCreation()));
		return photo;
	}

	public static PhotoCreateDTO convertToDTO(Photo photo) {
		PhotoCreateDTO photoDTO = new PhotoCreateDTO();
		photoDTO.setUrlPhoto(photo.getUrlPhoto());
		photoDTO.setDeleted(photo.isDeleted());
		// dto.setDateCreation(photo.getDateCreation().toString());
		return photoDTO;
	}

	// READ ALL
	public static List<PhotoCreateDTO> convertToDtoList(List<Photo> photoList) {
		List<PhotoCreateDTO> dtoList = new ArrayList<>();
		if (CollectionUtils.isEmpty(photoList)) {
			for (Photo photo : photoList) {
				dtoList.add(convertToDTO(photo));
			}
		}
		return dtoList;
	}
	
		
	//17/11
	public static List<Photo> convertToEntityList(List<PhotoCreateDTO> photodto) {
		List<Photo> entityList = new ArrayList<>();
		if (CollectionUtils.isEmpty(photodto)) {
			for (PhotoCreateDTO photodto2 : photodto) {
				entityList.add(convertToEntity(photodto2));
			}
		}
		return entityList;
	}

	
	
	
	
	public  static Photo convertToEntity(PhotoReadDTO photoDTO) {
		Photo photo = new Photo();
		
		photo.setUrlPhoto(photoDTO.getUrlPhoto());

		return photo;
	}

	


	
		public static List<Photo> convertToEntityListReadOnly (List<PhotoReadDTO> photodto) {
			List<Photo> entityList = new ArrayList<>();
			if (CollectionUtils.isEmpty(photodto)) {
				for (PhotoReadDTO photodto2 : photodto) {
					entityList.add(convertToEntity(photodto2));
				}
			}
			return entityList;
		}
	
	
	

}