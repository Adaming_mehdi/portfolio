package com.fr.inti.converter;
import com.fr.inti.dto.UserCreateDTO;
import com.fr.inti.dto.UserReadDTO;
import com.fr.inti.dto.UserUpdateDTO;
import com.fr.inti.entity.User;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserConverter {

    private UserConverter() {
    }

    public static User convertToCreateEntity(UserCreateDTO dto) {
        User user = new User();
        user.setNom(dto.getNom());
        user.setPrenom(dto.getPrenom());
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());
        user.setAvatarUrl(dto.getAvatarUrl() != null  ? dto.getAvatarUrl() : "");
        user.setPersonnalisation(dto.getPersonnalisation() != null  ? dto.getPersonnalisation() : "");
        user.setDateCreation(LocalDateTime.now());

        return user;
    }

    public static User convertToUpdateEntity(UserUpdateDTO dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setNom(dto.getNom());
        user.setPrenom(dto.getPrenom());
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());
        user.setAvatarUrl(dto.getAvatarUrl() != null ? dto.getAvatarUrl() : "");
        user.setPersonnalisation(dto.getPersonnalisation() != null ? dto.getPersonnalisation() : "");
        user.setDateCreation(LocalDateTime.now());
        return user;
    }
    public static UserReadDTO convertToReadDTO(User user) {
        UserReadDTO dto = new UserReadDTO();
        dto.setId(user.getId());
        dto.setNom(user.getNom());
        dto.setPrenom(user.getPrenom());
        dto.setLogin(user.getLogin());
        dto.setPassword(user.getPassword());
        dto.setAvatarUrl(user.getAvatarUrl() != null ? user.getAvatarUrl():"");
        dto.setPersonnalisation(user.getPersonnalisation() != null ? user.getPersonnalisation():"");
        dto.setDeleted(user.isDeleted());
        if(user.getDateCreation() != null) {
            dto.setDateCreation(user.getDateCreation().toString());
        }
        if(user.getMessages() != null) {
            dto.setMessages(MessageConverter.convertToDtoList(user.getMessages()));
        }
        if(user.getPortfolio() != null) {
            dto.setPortFolio(PortfolioConverter.convertToReadDTO(user.getPortfolio()));

        }
        return dto;
    }


    public static List<UserReadDTO> convertToDtoList(List<User> users) {
        List<UserReadDTO> dtoList = new ArrayList<>();
        if (!users.isEmpty()) {
            for (User user : users) {
                if(!user.isDeleted()) {
                    dtoList.add(convertToReadDTO(user));
                }
            }
        }
        return dtoList;

    }
}
