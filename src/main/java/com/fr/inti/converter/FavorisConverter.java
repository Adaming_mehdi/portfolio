package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;


import com.fr.inti.dto.FavorisCreateDTO;
import com.fr.inti.dto.FavorisReadDTO;
import com.fr.inti.entity.Favoris;



public class FavorisConverter{
	
		
		private FavorisConverter() {
		super();
		
	}
		public static FavorisCreateDTO  convertToDTO(Favoris favoris) {
			FavorisCreateDTO favorisDTO = new FavorisCreateDTO();
			favorisDTO.setId(favoris.getId());
			favorisDTO.setLibelle(favoris.getLibelle());
			 if((favoris.getDomaines() != null) && !favoris.getDomaines().isEmpty()) {
		            favorisDTO.setDomaines(DomaineConverter.convertToDtoList(favoris.getDomaines()));
		        }
		        if((favoris.getRecruteurs() != null) && !favoris.getRecruteurs().isEmpty()){
		            favorisDTO.setRecruteurs(RecruterConverter.convertToDtoList(favoris.getRecruteurs()));

		        }
			favorisDTO.setDeleted(favoris.isDeleted());
			favorisDTO.setDateCreation(favoris.getDateCreation());
			return favorisDTO;	
	
			
		}
		public  static Favoris convertToEntity(FavorisCreateDTO favorisDTO) {
			Favoris favoris = new Favoris();
			favoris.setId(favorisDTO.getId());
			favoris.setLibelle(favorisDTO.getLibelle());			
			if((favorisDTO.getDomaines() != null) && !favorisDTO.getDomaines().isEmpty()) {
	            favoris.setDomaines(DomaineConverter.convertToEntityList(favorisDTO.getDomaines()));
	        }
	        if((favorisDTO.getRecruteurs() != null) && !favorisDTO.getRecruteurs().isEmpty()) {
	            favoris.setRecruteurs(RecruterConverter.convertToEntityList(favorisDTO.getRecruteurs()));

	        }
			favoris.setDeleted(favorisDTO.isDeleted());
			favoris.setDateCreation(favorisDTO.getDateCreation());
			return favoris;
		}
		public static List<FavorisCreateDTO> convertToDtoList(List<Favoris> favoris) {
			List<FavorisCreateDTO> dtoList = new ArrayList<>();
			if (Objects.isNull(favoris)) {
				
				return dtoList; } else {
					
			if (!CollectionUtils.isEmpty(favoris)) {
				for (Favoris favorisO : favoris) {
					dtoList.add(convertToDTO(favorisO));
				}
			}
			return dtoList;
				}
		}
		public static List<Favoris> convertToEntityList(List<FavorisCreateDTO> favorisdto) {
			List<Favoris> entityList = new ArrayList<>();
			if (Objects.isNull(favorisdto)) {
				
				return entityList; } else {	
			
			if (!CollectionUtils.isEmpty(favorisdto)) {
				for (FavorisCreateDTO favorisdtoO : favorisdto) {
					entityList.add(convertToEntity(favorisdtoO));
				}
			}
			return entityList;
				}
		}
		public static FavorisReadDTO  convertToDTOReadOnly(Favoris favoris) {
			FavorisReadDTO favorisDTO = new FavorisReadDTO();
			favorisDTO.setLibelle(favoris.getLibelle());
			 if((favoris.getDomaines() != null) && !favoris.getDomaines().isEmpty()) {
		            favorisDTO.setDomaines(DomaineConverter.convertToDtoList(favoris.getDomaines()));
		        }
		        if((favoris.getRecruteurs() != null) && !favoris.getRecruteurs().isEmpty()) {
		            favorisDTO.setRecruteurs(RecruterConverter.convertToDtoList(favoris.getRecruteurs()));

		        }
			return favorisDTO;	
	
			
		}
		public  static Favoris convertToEntity(FavorisReadDTO favorisDTO) {
			Favoris favoris = new Favoris();
			
			favoris.setLibelle(favorisDTO.getLibelle());
			if((favorisDTO.getDomaines() != null) && !favorisDTO.getDomaines().isEmpty()) {
	            favoris.setDomaines(DomaineConverter.convertToEntityList(favorisDTO.getDomaines()));
	        }
	        if((favorisDTO.getRecruteurs() != null) && !favoris.getRecruteurs().isEmpty()) {
	            favoris.setRecruteurs(RecruterConverter.convertToEntityList(favorisDTO.getRecruteurs()));

	        }
			return favoris;
		}
		public static List<FavorisReadDTO> convertToDtoListReadOnly(List<Favoris> favoris) {
			List<FavorisReadDTO> dtoList = new ArrayList<>();
			if (Objects.isNull(favoris)) {
			
			return dtoList; } else {
		
			
			if (!CollectionUtils.isEmpty(favoris)) {
				for (Favoris favorisO : favoris) {
					dtoList.add(convertToDTOReadOnly(favorisO));
				}
			}
			return dtoList;}
		}
		public static List<Favoris> convertToEntityListReadOnly (List<FavorisReadDTO> favorisdto) {
			List<Favoris> entityList = new ArrayList<>();
			if (Objects.isNull(favorisdto)) {
				
				return entityList; } else {
			
			if (!CollectionUtils.isEmpty(favorisdto)) {
				for (FavorisReadDTO favorisdtoO : favorisdto) {
					entityList.add(convertToEntity(favorisdtoO));
				}
			}
			return entityList;
		}
		
}}
