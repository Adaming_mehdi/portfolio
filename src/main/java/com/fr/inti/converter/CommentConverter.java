package com.fr.inti.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fr.inti.dto.CommentCreateDTO;
import com.fr.inti.dto.CommentReadDTO;
import com.fr.inti.dto.CommentUpdateDTO;
import com.fr.inti.entity.Comment;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.entity.Recruter;

public class CommentConverter {

// READ CONVERTER

	private CommentConverter() {
	}

	public static Comment convertToEntity(CommentReadDTO dto) {
		if (dto == null)
			return null;

		Comment comment = new Comment();

		comment.setId(dto.getId());
		comment.setContentComment(dto.getContentComment());
		if (dto.getRecruter() != null) {
			Recruter recruter = new Recruter();
			recruter.setId(dto.getRecruter().getId());
			comment.setRecruter(recruter);
		}
		if (dto.getPortfolio() != null) {
			Portfolio portfolio = new Portfolio();
			portfolio.setId(dto.getPortfolio().getId());
			comment.setPortfolio(portfolio);
		}
		comment.setDeleted(dto.isDeleted());
		comment.setDateCreation(LocalDateTime.parse(dto.getDateCreation()));

		return comment;
	}

	public static CommentReadDTO convertToReadDTO(Comment comment) {
		if (comment == null)
			return null;

		CommentReadDTO dto = new CommentReadDTO();

		dto.setId(comment.getId());
		dto.setContentComment(comment.getContentComment());
		if (comment.getRecruter() != null)
			dto.setRecruter(RecruterConverter.convertToDTO(comment.getRecruter()));
		if (comment.getPortfolio() != null)
			dto.setPortfolio(PortfolioConverter.convertToReadDTO(comment.getPortfolio()));
		dto.setDeleted(comment.isDeleted());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("'Créé le' yyyy-MM-dd 'à' HH:mm:ss");
		dto.setDateCreation(comment.getDateCreation().format(formatter));

		return dto;
	}

// READ ALL CONVERTER

	public static List<Comment> convertToEntityList(List<CommentReadDTO> dtoList) {
		if (dtoList == null)
			return Collections.emptyList();

		List<Comment> entityList = new ArrayList<>();
		for (CommentReadDTO commentDTO : dtoList) {
			entityList.add(convertToEntity(commentDTO));
		}

		return entityList;
	}

	public static List<CommentReadDTO> convertToDtoList(List<Comment> commentList) {
		if (commentList == null)
			return Collections.emptyList();

		List<CommentReadDTO> dtoList = new ArrayList<>();
		for (Comment comment : commentList) {
			dtoList.add(convertToReadDTO(comment));
		}

		return dtoList;
	}

// CREATE CONVERTER

	public static Comment convertToEntity(CommentCreateDTO dto) {
		if (dto == null)
			return null;

		Comment comment = new Comment();

		comment.setContentComment(dto.getContentComment());
		if (dto.getIdRecruter() != null) {
			Recruter recruter = new Recruter();
			recruter.setId(dto.getIdRecruter());
			comment.setRecruter(recruter);
		}
		if (dto.getIdPortfolio() != null) {
			Portfolio portfolio = new Portfolio();
			portfolio.setId(dto.getIdPortfolio());
			comment.setPortfolio(portfolio);
		}

		return comment;
	}

	public static CommentCreateDTO convertToCreateDTO(Comment comment) {
		if (comment == null)
			return null;

		CommentCreateDTO dto = new CommentCreateDTO();

		dto.setContentComment(comment.getContentComment());
		dto.setIdRecruter(comment.getRecruter().getId());
		dto.setIdPortfolio(comment.getPortfolio().getId());

		return dto;
	}

// UPDATE CONVERTER

	public static Comment convertToEntity(CommentUpdateDTO dto) {
		if (dto == null)
			return null;

		Comment comment = new Comment();

		comment.setId(dto.getId());
		comment.setContentComment(dto.getContentComment());
		if (dto.getIdRecruter() != null) {
			Recruter recruter = new Recruter();
			recruter.setId(dto.getIdRecruter());
			comment.setRecruter(recruter);
		}
		if (dto.getIdPortfolio() != null) {
			Portfolio portfolio = new Portfolio();
			portfolio.setId(dto.getIdPortfolio());
			comment.setPortfolio(portfolio);
		}

		return comment;
	}

	public static CommentUpdateDTO convertToUpdateDTO(Comment comment) {
		if (comment == null)
			return null;

		CommentUpdateDTO dto = new CommentUpdateDTO();

		dto.setId(comment.getId());
		dto.setContentComment(comment.getContentComment());
		dto.setIdRecruter(comment.getRecruter().getId());
		dto.setIdPortfolio(comment.getPortfolio().getId());

		return dto;
	}

}
