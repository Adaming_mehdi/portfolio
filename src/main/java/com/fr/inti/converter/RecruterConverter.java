package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.List;

import com.fr.inti.dto.RecruterCreateDTO;
import com.fr.inti.dto.RecruterReadDTO;
import com.fr.inti.entity.Recruter;

public class RecruterConverter {

	private RecruterConverter() {
	}

	public static Recruter convertCreateDTOToEntity(RecruterCreateDTO dto) {
		if (dto == null) {
			return null;
		}
		Recruter recruter = new Recruter();
		recruter.setNom(dto.getNom());
		recruter.setPrenom(dto.getPrenom());
		recruter.setLogin(dto.getLogin());
		recruter.setPassword(dto.getPassword());
		recruter.setAvatarUrl(dto.getAvatarUrl());
		return recruter;
	}

	public static Recruter convertReadDTOToEntity(RecruterReadDTO dto) {
		if (dto == null) {
			return null;
		}
		Recruter recruter = new Recruter();
		recruter.setNom(dto.getNom());
		recruter.setPrenom(dto.getPrenom());
		recruter.setLogin(dto.getLogin());
		recruter.setPassword(dto.getPassword());
		recruter.setAvatarUrl(dto.getAvatarUrl());
		if (dto.getIdmessages() != null) {
			recruter.setMessages(MessageConverter.convertToEntityList(dto.getIdmessages()));
		}
		if (dto.getIdcomments() != null) {
			recruter.setComments(CommentConverter.convertToEntityList(dto.getIdcomments()));
		}
		if (dto.getIdfavoris() != null) {
			recruter.setFavoris(FavorisConverter.convertToEntity(dto.getIdfavoris()));
		}
		return recruter;
	}

	public static RecruterReadDTO convertToDTO(Recruter recruter) {
		if (recruter == null) {
			return null;
		}
		RecruterReadDTO dto = new RecruterReadDTO();
		dto.setId(recruter.getId());
		dto.setNom(recruter.getNom());
		dto.setPrenom(recruter.getPrenom());
		dto.setLogin(recruter.getLogin());
		dto.setPassword(recruter.getPassword());
		dto.setAvatarUrl(recruter.getAvatarUrl());
		if (recruter.getMessages() != null) {
			dto.setIdmessages(MessageConverter.convertToDtoList(recruter.getMessages()));
		}
		if (recruter.getComments() != null) {
			dto.setIdcomments(CommentConverter.convertToDtoList(recruter.getComments()));
		}

		dto.setDeleted(recruter.isDeleted());
		if (recruter.getDateCreation() != null) {
			dto.setDateCreation(recruter.getDateCreation().toString());
		}
		return dto;
	}

	public static List<RecruterReadDTO> convertToDtoList(List<Recruter> recruterList) {
		List<RecruterReadDTO> dtoList = new ArrayList<>();
		for (Recruter  recrut : recruterList) {
			dtoList.add(convertToDTO(recrut));
		}
		return dtoList;
	}

	public static List<Recruter> convertToEntityList(List<RecruterReadDTO> recruterList) {
		List<Recruter> entityList = new ArrayList<>();
		for (RecruterReadDTO recruterdto : recruterList) {
			entityList.add(convertReadDTOToEntity(recruterdto));
		}
		return entityList;
	}

}
