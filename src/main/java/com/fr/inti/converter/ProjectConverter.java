package com.fr.inti.converter;


import com.fr.inti.dto.ProjectCreateDTO;
import com.fr.inti.dto.ProjectReadDTO;
import com.fr.inti.entity.*;


import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;


import org.springframework.util.CollectionUtils;


import java.util.ArrayList;
import java.util.List;;

public class ProjectConverter {
	

	
	private ProjectConverter() {
		super();
		
	}

	public static Project convertToReadEntity(ProjectReadDTO dto)  {
		
		Project project = new Project();
		Domaine domaine = new Domaine();
		Organisation organisation = new Organisation();
		Portfolio portfolio = new Portfolio();
		Photo photo = new Photo();
		if (dto == null) {
			return null;
		}
		if (dto.getDomaine_id() != null) {

			domaine.setId(dto.getDomaine_id());
		}
		if (dto.getOrganisation_id() != null) {

			organisation.setId(dto.getOrganisation_id());
		}
		if (dto.getPortfolio_id() != null) {

			portfolio.setId(dto.getPortfolio_id());
		}
		if (dto.getPhoto_id() != null) {

			photo.setId(dto.getPhoto_id());
		}
		
		
		project.setId(dto.getId());
		project.setNom(dto.getNom());
		project.setDescription(dto.getDescription());
		project.setDateDebut(LocalDate.parse(dto.getDateDebut()));
		project.setDateFin(LocalDate.parse(dto.getDateFin()));
		if (domaine != null) {
			project.setDomaine(domaine);
		}
		if (organisation != null) {
			project.setOrganisation(organisation);
		}
		if (portfolio != null) {
			project.setPortfolio(portfolio);
		}
		if (photo != null) {
			project.setPhoto(photo);
		}
		project.setDeleted(dto.isDeleted());
		if (dto.getDateCreation() != null) {
			project.setDateCreation(LocalDateTime.parse(dto.getDateCreation()));
		}
		
		return project;
		
	}
	
	public static ProjectReadDTO convertToReadDTO(Project project) {
		ProjectReadDTO dto = new ProjectReadDTO();
		
		if (project == null) {
			return null;
		}
		
		dto.setId(project.getId());
		dto.setNom(project.getNom());
		dto.setDescription(project.getDescription());
		dto.setDateDebut(project.getDateDebut().toString());
		dto.setDateFin(project.getDateFin().toString());
		if (project.getDomaine() != null) {
			dto.setDomaine_id(project.getDomaine().getId());
		}
		if (project.getOrganisation() != null) {
			dto.setOrganisation_id(project.getOrganisation().getId());
		}
		if (project.getPortfolio() != null) {
			dto.setPortfolio_id(project.getPortfolio().getId());
		}
		
		if (project.getPhoto() != null) {
			dto.setPhoto_id(project.getPhoto().getId());
		}
		
		dto.setDeleted(project.isDeleted());
		if (project.getDateCreation() != null) {
			dto.setDateCreation(project.getDateCreation().toString());
		}
		
		return dto;
	}
	
//	READ ALL CONVERTER
	
	public static List<ProjectReadDTO> convertToDtoList(List<Project> projectList) {
		List<ProjectReadDTO> dtoList = new ArrayList<>();
		if (CollectionUtils.isEmpty(projectList)) {
			for (Project project : projectList) {
				dtoList.add(convertToReadDTO(project));
			}
		}
		return dtoList;
	}
	
// CREATE CONVERTER
	
	public static Project convertToCreateEntity(ProjectCreateDTO dto) {
		
		Project project = new Project();
		Domaine domaine = new Domaine();
		Organisation organisation = new Organisation();
		Portfolio portfolio = new Portfolio();
		Photo photo = new Photo();
		if (dto == null) {
			return null;
		}
		if (dto.getDomaine_id() != null) {

			domaine.setId(dto.getDomaine_id());
		}
		if (dto.getOrganisation_id() != null) {

			organisation.setId(dto.getOrganisation_id());
		}
		if (dto.getPortfolio_id() != null) {

			portfolio.setId(dto.getPortfolio_id());
		}
		if (dto.getPhoto_id() != null) {

			photo.setId(dto.getPhoto_id());
		}
		
		
	
		project.setNom(dto.getNom());
		project.setDescription(dto.getDescription());
		project.setDateDebut(LocalDate.parse(dto.getDateDebut()));
		project.setDateFin(LocalDate.parse(dto.getDateFin()));
		if (domaine != null) {
			project.setDomaine(domaine);
		}
		if (organisation != null) {
			project.setOrganisation(organisation);
		}
		if (portfolio != null) {
			project.setPortfolio(portfolio);
		}
		if (photo != null) {
			project.setPhoto(photo);
		}
	
		return project;
		
	}
	public static ProjectReadDTO convertToCreateDTO(Project project) {
		ProjectReadDTO dto = new ProjectReadDTO();
		
		if (project == null) {
			return null;
		}
		
	
		dto.setNom(project.getNom());
		dto.setDescription(project.getDescription());
		dto.setDateDebut(project.getDateDebut().toString());
		dto.setDateFin(project.getDateFin().toString());
		if (project.getDomaine() != null) {
			dto.setDomaine_id(project.getDomaine().getId());
		}
		if (project.getOrganisation() != null) {
			dto.setOrganisation_id(project.getOrganisation().getId());
		}
		if (project.getPortfolio() != null) {
			dto.setPortfolio_id(project.getPortfolio().getId());
		}
		
		if (project.getPhoto() != null) {
			dto.setPhoto_id(project.getPhoto().getId());
		}
	
		return dto;
	}
	
	public static List<Project> convertToReadListEntity(List<ProjectReadDTO> dtoList){
		List<Project> projectList = new ArrayList<Project>();
		for (ProjectReadDTO dto : dtoList) {
			projectList.add(convertToReadEntity(dto));
		}
		return projectList;
		
	}
	
 	
}
