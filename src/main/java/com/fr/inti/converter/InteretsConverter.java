package com.fr.inti.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.fr.inti.dto.InteretsCreateDTO;
import com.fr.inti.dto.InteretsReadDTO;
import com.fr.inti.entity.Interets;
import com.fr.inti.entity.Photo;
import com.fr.inti.entity.Portfolio;

public class InteretsConverter {
	
	
	
	private InteretsConverter() {}

	public static InteretsReadDTO convertToDTO(Interets entity){
		
		InteretsReadDTO dto = new InteretsReadDTO();
		
		if(entity == null) {
			return null;
		}
		
		if(entity.getDateCreation() != null) {
			
			dto.setDateCrea(entity.getDateCreation().toString());
		}
		
		dto.setId(entity.getId());
		dto.setDescription(entity.getDescription());
		
		

		if(entity.getPhoto() != null) {
			
			dto.setIdPhoto(entity.getPhoto().getId());
		}
			
		return dto;
	}
	
	public static Interets convertToEntity(InteretsCreateDTO dto){
		
		Interets entity = new Interets();
		
		if(dto == null) {
			return null;
		}
		
		
		entity.setDescription(dto.getDescription());
		
		if(dto.getIdPortfolio() != null) {
			Portfolio port = new Portfolio();
			port.setId(dto.getIdPortfolio());
			entity.setPortfolio(port);
		}
		
		if(dto.getIdPhoto() != null) {
			
			Photo photo = new Photo();
			photo.setId(dto.getIdPhoto());
			entity.setPhoto(photo);
		}
		
		return entity;
		
	}
	
public static List<InteretsReadDTO> convertToListDTO(List<Interets> lstentity){
		
		
		if(lstentity == null) {
			return null;
		}
		
		return lstentity.stream().map(InteretsConverter::convertToDTO).collect(Collectors.toList());
				
	}

}
