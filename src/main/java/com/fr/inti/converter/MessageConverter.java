package com.fr.inti.converter;

import java.time.LocalDateTime;


import java.util.ArrayList;
import java.util.List;


import com.fr.inti.dto.MessageUpdateDTO;
import com.fr.inti.dto.MessageCreateDTO;
import com.fr.inti.dto.MessageReadDTO;
import com.fr.inti.entity.Message;
import com.fr.inti.entity.Recruter;
import com.fr.inti.entity.User;;

public class MessageConverter {
	
	public static Message convertToEntity(MessageReadDTO dto) {
		if (dto != null) {
			Message message = new Message();
			if (dto.getContentMessage()!= null) {
				message.setContentMessage(dto.getContentMessage());
				}
			
			if (dto.getIdRecruter()!= null){
				Recruter recruter = new Recruter();
				recruter.setId(dto.getIdRecruter());
				message.setRecruter(recruter);
				}
			
			if (dto.getIdUser() != null) {
				User user = new User();
				user.setId(dto.getIdUser());
				message.setUser(user);
				}
		
		message.setDeleted(dto.isDeleted());
		message.setDateCreation(LocalDateTime.parse(dto.getDateCreation()));
		
		return message;
		}
		else 
			return null;
	}
	
	public static MessageReadDTO convertToReadDTO(Message message) {
		if (message != null) {
		MessageReadDTO dto = new MessageReadDTO();

			if (message.getContentMessage()!= null) {
				dto.setContentMessage(message.getContentMessage());
				}
			
			if (message.getRecruter()!= null){
				Recruter recruter = new Recruter();
				dto.setIdRecruter(message.getRecruter().getId());
				recruter.setId(message.getId());
				}
			
			if (message.getUser() != null) {
				User user = new User();
				user.setId(message.getId());
				dto.setIdUser(message.getUser().getId());
				}
		dto.setDeleted(message.isDeleted());
		dto.setDateCreation(message.getDateCreation().toString());
		return dto;
	}
		else 
			return null;
	}

// READ ALL CONVERTER
	public static List<Message> convertToEntityList(List<MessageReadDTO> dtoList) {
		if (dtoList == null)
			return null; 
		
		List<Message> entityList = new ArrayList<>();
		for (MessageReadDTO messageDTO : dtoList) {
			entityList.add(convertToEntity(messageDTO));
		}
		return entityList;
	}
	
	public static List<MessageReadDTO> convertToDtoList(List<Message> messageList) {
		if (messageList == null)
			return null;
		
		List<MessageReadDTO> dtoList = new ArrayList<>();
			for (Message message : messageList) {
				dtoList.add(convertToReadDTO(message)  );
			
		}
		return dtoList;
	}

// CREATE CONVERTER

	public static Message convertToEntity(MessageCreateDTO dto) {
		if (dto != null) {
		Message message = new Message();
		if (dto.getContentMessage()!= null) {
			message.setContentMessage(dto.getContentMessage());
			}
		
		if (dto.getIdRecruter()!= null){
			Recruter recruter = new Recruter();
			recruter.setId(dto.getIdRecruter());
			message.setRecruter(recruter);
			}
		
		if (dto.getIdUser() != null) {
			User user = new User();
			user.setId(dto.getIdUser());
			message.setUser(user);
			}
		

		return message;
	}
		else 
			return null;
		
	}

	public static MessageCreateDTO convertToCreateDTO(Message message) {
		if (message != null) {
		MessageCreateDTO dto = new MessageCreateDTO();
		if (message.getContentMessage()!= null) {
			dto.setContentMessage(message.getContentMessage());
			}
		
		if (message.getRecruter()!= null){
			Recruter recruter = new Recruter();
			dto.setIdRecruter(message.getRecruter().getId());
			recruter.setId(message.getId());
			}
		
		if (message.getUser() != null) {
			User user = new User();
			user.setId(message.getId());
			dto.setIdUser(message.getUser().getId());
			}
		
		
		return dto;
	}
		else 
			return null;
	}
	
	// UPDATE CONVERTER

		public static Message convertToEntity(MessageUpdateDTO dto) {
			if (dto != null) {
				Message message = new Message();
				if (dto.getContentMessage()!= null) {
					message.setContentMessage(dto.getContentMessage());
					message.getId();
				}
				
				if (dto.getIdRecruter()!= null){
					Recruter recruter = new Recruter();
					recruter.setId(dto.getIdRecruter());
					message.setRecruter(recruter);
					
					}
				
				if (dto.getIdUser() != null) {
					User user = new User();
					user.setId(dto.getIdUser());
					message.setUser(user);
					}
				
				if(dto.getId() != null) {
					

				}
			return message;
		}
			else 
				return null; 
		}

		public static MessageUpdateDTO convertToUpdateDTO(Message message) {
			MessageUpdateDTO dto = new MessageUpdateDTO();

			
			if (message != null) {
				if (message.getContentMessage()!= null) {
					dto.setContentMessage(message.getContentMessage());
					dto.setId(message.getId());
					}
				
				if (message.getRecruter()!= null){
					Recruter recruter = new Recruter();
					dto.setIdRecruter(message.getRecruter().getId());
					recruter.setId(message.getId());
					}
				
				if (message.getUser() != null) {
					User user = new User();
					user.setId(message.getId());
					dto.setIdUser(message.getUser().getId());
					}
				
				
				return dto;
			}
				else 
					return null;
			}


}
