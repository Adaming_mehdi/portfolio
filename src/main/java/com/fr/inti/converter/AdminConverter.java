package com.fr.inti.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;

import com.fr.inti.dto.AdminCreateDTO;
import com.fr.inti.dto.AdminReadDTO;
import com.fr.inti.dto.AdminUpdateDTO;
import com.fr.inti.entity.Admin;

public class AdminConverter {
	
	

	private AdminConverter() {}

	public static Admin convertToCreateEntity(AdminCreateDTO dto) {

		if (Objects.isNull(dto)) {

			return null;

		}
		Admin admin = new Admin();

		admin.setLogin(dto.getLogin());
		admin.setNom(dto.getNom());
		admin.setPrenom(dto.getPrenom());
		admin.setPassword(dto.getPassword());
		admin.setAvatarUrl(dto.getAvatarUrl());

		return admin;

	}

	public static Admin convertToUpdateEntity(AdminUpdateDTO dto) {

		if (Objects.isNull(dto)) {

			return null;
		}

		Admin admin = new Admin();

		admin.setId(dto.getId());
		admin.setLogin(dto.getLogin());
		admin.setNom(dto.getNom());
		admin.setPrenom(dto.getPrenom());
		admin.setPassword(dto.getPassword());
		admin.setAvatarUrl(dto.getAvatarUrl());
		admin.setDeleted(dto.isDeleted());

		return admin;

	}

	public static AdminReadDTO convertToReadDTO(Admin admin) {

		if (Objects.isNull(admin)) {

			return null;

		}

		AdminReadDTO dto = new AdminReadDTO();

		dto.setId(admin.getId());
		dto.setLogin(admin.getLogin());
		dto.setNom(admin.getNom());
		dto.setPrenom(admin.getPrenom());
		if (!Objects.isNull(admin.getDateCreation())) {
			dto.setDateCreation(admin.getDateCreation().toString());
		}
		dto.setDeleted(admin.isDeleted());
		dto.setAvatarUrl(admin.getAvatarUrl());

		return dto;

	}

	public static List<AdminReadDTO> convertToReadDtoList(List<Admin> admins) {

		if (Objects.isNull(admins)) {

			return null;

		}

		List<AdminReadDTO> dtoList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(admins)) {
			for (Admin admin : admins) {
				dtoList.add(convertToReadDTO(admin));
			}
		}

		return dtoList;
	}

}
