package com.fr.inti.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.fr.inti.dto.EtudeCreateDTO;
import com.fr.inti.dto.EtudeReadDTO;
import com.fr.inti.dto.EtudeUpdateDTO;
import com.fr.inti.entity.Domaine;
import com.fr.inti.entity.Etude;
import com.fr.inti.entity.Organisation;
import com.fr.inti.entity.Photo;
import com.fr.inti.entity.Portfolio;

public class EtudeConverter {

	private EtudeConverter() {
	}

	public static Etude convertToEntity(EtudeCreateDTO dto) {
		if (dto != null) {
			Etude entity = new Etude();
			entity.setNom(dto.getNom());
			entity.setDescription(dto.getDescription());
			entity.setDateDebut(LocalDate.parse(dto.getDateDebut()));
			entity.setDateFin(LocalDate.parse(dto.getDateFin()));
			if (dto.getDomaineId() != null) {
				Domaine domaine = new Domaine();
				domaine.setId(dto.getDomaineId());
				entity.setDomaine(domaine);
			}
			if (dto.getOrganisationId() != null) {
				entity.setOrganisation(new Organisation(dto.getOrganisationId()));
			}
			if (dto.getPortfolioId() != null) {
				Portfolio portfolio = new Portfolio();
				portfolio.setId(dto.getPortfolioId());
				entity.setPortfolio(portfolio);
			}
			if (dto.getPhotoId() != null) {
				Photo photo = new Photo();
				photo.setId(dto.getPhotoId());
				entity.setPhoto(photo);
			}
			return entity;
		}
		
		return null;
	}
	
	public static Etude convertToEntity(EtudeUpdateDTO dto, Etude originEntity) {
		if (dto != null && originEntity != null) {
			Etude entity = originEntity;
			entity.setNom(dto.getNom());
			entity.setDescription(dto.getDescription());
			entity.setDateDebut(LocalDate.parse(dto.getDateDebut()));
			entity.setDateFin(LocalDate.parse(dto.getDateFin()));
			if (dto.getDomaineId() != null) {
				Domaine domaine = new Domaine();
				domaine.setId(dto.getDomaineId());
				entity.setDomaine(domaine);
			}
			if (dto.getOrganisationId() != null) {
				entity.setOrganisation(new Organisation(dto.getOrganisationId()));
			}
			if (dto.getPortfolioId() != null) {
				Portfolio portfolio = new Portfolio();
				portfolio.setId(dto.getPortfolioId());
				entity.setPortfolio(portfolio);
			}
			if (dto.getPhotoId() != null) {
				Photo photo = new Photo();
				photo.setId(dto.getPhotoId());
				entity.setPhoto(photo);
			}
			return entity;
		}
		
		return null;
	}
	
	public static EtudeReadDTO convertToDto(Etude entity) {
	
		if (entity != null) {
			EtudeReadDTO dto = new EtudeReadDTO();
			dto.setId(entity.getId());
			dto.setNom(entity.getNom());
			dto.setDescription(entity.getDescription());
			dto.setDateDebut(entity.getDateDebut().toString());
			dto.setDateFin(entity.getDateFin().toString());
			if (entity.getDomaine() != null) {
				dto.setNomDomaine(entity.getDomaine().getLibelle());
			}
			if (entity.getOrganisation() != null) {
				dto.setNomOrganisation(entity.getOrganisation().getNom());
			}
			if (entity.getPhoto() != null) {
				dto.setPhotoUrl(entity.getPhoto().getUrlPhoto());
			}
			dto.setDeleted(entity.isDeleted());
			return dto;
		}
		return null;
	}
	
	public static List<EtudeReadDTO> convertToDtoList(List<Etude> entityList) {
		List<EtudeReadDTO> dtoList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(entityList)) {
			for (Etude entity : entityList) {
				dtoList.add(convertToDto(entity));
			}
		}
		return dtoList;
	}

}
