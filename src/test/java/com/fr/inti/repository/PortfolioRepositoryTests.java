package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Portfolio;

@SpringBootTest
class PortfolioRepositoryTests {

	@Autowired
	private PortfolioRepository repo;

	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

	private final static long idEntity = 1L;

	@Test
	@Sql(statements = { "DELETE FROM portfolio" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testCreatingPortfolio_shouldHaveCreatedDateNotNull() {
		Portfolio portfolio = new Portfolio();
		portfolio.setVisible(true);

		Portfolio entity = repo.save(portfolio);

		assertThat(entity).isNotNull();
		assertThat(entity.getId()).isNotNull();
		assertThat(entity.getDateCreation()).isNotNull();
		assertThat(entity.isDeleted()).isFalse();
		assertThat(entity.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());
	}

	@Test
	@Sql(statements = { "DELETE FROM portfolio" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdatingPortfolioById_shouldNotUpdateCreatedDate() {
		Portfolio portfolio = new Portfolio();
		portfolio.setVisible(true);

		Portfolio entity = repo.save(portfolio);

		assertThat(entity).isNotNull().hasFieldOrPropertyWithValue("visible", true)
				.hasFieldOrPropertyWithValue("deleted", false);
		assertThat(entity.getId()).isNotNull().isNotNegative();
		assertThat(entity.getDateCreation()).isNotNull();
		assertThat(entity.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());

		String date = entity.getDateCreation().toString();

// update field

		entity.setVisible(false);
		entity.setDeleted(true);

		Portfolio entity2 = repo.save(entity);

		assertThat(entity2.getId()).isNotNull().isEqualTo(entity.getId());
		assertThat(entity2).isNotNull().hasFieldOrPropertyWithValue("visible", false)
				.hasFieldOrPropertyWithValue("deleted", true);
		assertThat(entity2.getDateCreation()).isNotNull();
		assertThat(entity2.getDateCreation()).asString().isEqualTo(date);
	}

	@Test
	@Sql(statements = { "INSERT INTO portfolio (id, visible) VALUES (1, true)",
			"INSERT INTO portfolio (id, visible) VALUES (2, false)",
			"INSERT INTO portfolio (id, visible) VALUES (3, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM portfolio" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testVisibleListValidEntity_shouldShowOnlyPortfolioWithTrueVisibleColumn() {

		List<Portfolio> listRepo = repo.findByVisibleTrue();

		assertThat(listRepo).isNotEmpty().isNotNull().size().isEqualTo(2);
		assertThat(listRepo.get(0).getId()).isEqualTo(1L);
		assertThat(listRepo.get(0).getVisible()).isTrue();
		assertThat(listRepo.get(1).getId()).isEqualTo(3L);
		assertThat(listRepo.get(1).getVisible()).isTrue();
	}

	@Test
	@Sql(statements = { "INSERT INTO portfolio (id, visible) VALUES (" + idEntity
			+ ", true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"DELETE FROM portfolio WHERE id = " + idEntity }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteValidEntity_shouldUpdateDeletedColumnOfGivenClass() {

		repo.fakeDelete(idEntity);

		Portfolio response = repo.findById(idEntity).get();

		assertThat(response.isDeleted()).isTrue();
	}

	@Test
	void testFakeDeleteByNegativeId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(-2L);
	}

	@Test
	void testFakeDeleteByZeroId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(0L);
	}

	@Test
	void testFakeDeleteByNullId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(null);
	}

	@Test
	void testFakeDeleteByNotExistingEntity_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(1L);
	}

}
