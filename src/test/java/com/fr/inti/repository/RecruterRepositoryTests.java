package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Recruter;

@SpringBootTest
public class RecruterRepositoryTests {

	@Autowired
	private RecruterRepository repo;
	private final static long ID_ENTITY_POSITIF = 1L;
//	private final static long ID_ENTITY_NEGATIF = -1L;

	@Test
	// Context
	@Sql(statements = "INSERT INTO recruter (id, nom, prenom, login, password, deleted) VALUES (" + ID_ENTITY_POSITIF + ", 'Nomdetest', 'Prenomdetest', 'Logindetest', 'Pswdetest', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM recruter WHERE id = " + ID_ENTITY_POSITIF, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteByPositiveId_shouldSetDeletedToTrue() {
		// Test
		repo.fakeDelete(ID_ENTITY_POSITIF);

		// Assert
		Recruter response = repo.findById(ID_ENTITY_POSITIF).get();
		assertThat(response.isDeleted()).isTrue();
	}
	
//	@Test
//	// Context
//	@Sql(statements = "INSERT INTO recruter (id, nom, prenom, login, password, avatarUrl, deleted) VALUES (" + ID_ENTITY_NEGATIF + ", 'Nomdetest', 'Prenomdetest', 'Logindetest', 'Pswdetest', 'Avatardetest', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "DELETE FROM recruter WHERE id = " + ID_ENTITY_NEGATIF, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	void testFakeDeleteByNegativeId_should() {
//		// Test
//		repo.fakeDelete(ID_ENTITY_NEGATIF);
//
//		// Assert
//		Recruter response = repo.findById(ID_ENTITY_NEGATIF).get();
//		assertThat(response.isDeleted()).isNull();
//	}
	
	@Test
	void testFakeDeleteByNegativeId_shoudNotThrowException() {
		repo.fakeDelete(-1L);
	}
	
	
	@Test
	void testFakeDeleteByZeroId_shoudNotThrowException() {
		repo.fakeDelete(0L);
	}

	@Test
	void testFakeDeleteByNullId_shoudNotThrowException() {
		repo.fakeDelete(null);
	}
	@Test
	void testFakeDeleteByNoExistingId_shoudNotThrowException() {
		repo.fakeDelete(1L);
	}

	
}
