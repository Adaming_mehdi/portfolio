package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Favoris;

@SpringBootTest
 class FavorisRepositoryTests {
	@Autowired
	private FavorisRepository repo;
	
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

	

	@BeforeEach
	void beforeTouteMethode() {
		System.err.println("-------------------------Before each-------------------------");
	}

	@AfterAll
	@Sql(statements = { "DELETE FROM favoris" })
	static void afterTouteMethode() {
		System.err.println("-------------------------After each-------------------------");
	}
	@Test
	@Sql(statements = { "DELETE FROM favoris" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testCreatingFavoris_shouldHaveCreatedDateNotNull() {
		Favoris favoris = new Favoris();
		favoris.setLibelle("le test");
		

		Favoris entity = repo.save(favoris);

		assertThat(entity).isNotNull();
		assertThat(entity.getId()).isNotNull();
		assertThat(entity.getDateCreation()).isNotNull();
		assertThat(entity.isDeleted()).isFalse();
		assertThat(entity.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());
	}

	
	@Test
	@Sql(statements = {"INSERT INTO favoris(id,libelle) VALUES('99','testfakedelete')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements =  {"DELETE FROM favoris WHERE id = '99'"},executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDelete_shouldAssignTruetoDeleted() {
				
		repo.fakeDelete(99L);
		Favoris fav = repo.findById(99L).get();
		
		assertEquals(true, fav.isDeleted());
		
	}
	
}
