package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Domaine;

@SpringBootTest
class DomaineRepositoryTests {
	@Autowired
	private DomaineRepository repo;

	@Test // Validity
	@Sql(statements = "INSERT INTO domaine (id, libelle, deleted) VALUES (55, 'libelleTest', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from domaine where id = 55", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteValidEnity_shoudUpdateDeletedColumnOfGivenEntity() {
		repo.fakeDelete(55L);
		Domaine response = repo.findById(55L).get();
		assertThat(response.isDeleted()).isTrue();
		assertThat(response.getLibelle()).isEqualTo("libelleTest");
	}

	@Test
	void testUpdateDeleteInvalidIds_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(null);
		repo.fakeDelete(-55L);

	}

}
