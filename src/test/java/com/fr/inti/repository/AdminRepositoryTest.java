package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Admin;

@SpringBootTest
class AdminRepositoryTest {

	@Autowired
	private AdminRepository repo;

	@Test
	@Sql(statements = "INSERT INTO admin (id, deleted, login, password) VALUES (1, false,'test','test')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM admin WHERE id =1;", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteValidEntity_shoudUpdateDeletedColumnOfGivenEntity() {

		repo.fakeDelete(1L);

		Admin response = repo.findById(1L).get();

		assertThat(response.isDeleted()).isTrue();

	}

}
