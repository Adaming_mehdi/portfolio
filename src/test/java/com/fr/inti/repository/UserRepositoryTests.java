package com.fr.inti.repository;
import com.fr.inti.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserRepositoryTests {

    @Autowired
    private UserRepository repo;


    @Test
    @Sql(statements = "INSERT INTO user (id, login, password, deleted) VALUES (50, 'test','test',false)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "delete from user where id = 50", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testFakeDeleteValidEnity_shoudUpdateDeletedColumnOfGivenEntity() {
        repo.updateDelete(50L);
        User response = repo.findById(50L).get();
        assertThat(response.isDeleted()).isTrue();
    }

    @Test
    @Sql(statements = "INSERT INTO user (id, login, password, deleted) VALUES (50, 'werss83','test',false)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "delete from user where id = 50", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testFindEtudeByOrganisation() {

        User user = repo.findByLogin("werss83");
        assertEquals(user.getId(), 50);
        assertEquals(user.getLogin(), "werss83");

    }


}
