package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Organisation;

@SpringBootTest
class OrganisationRepositoryTests {

	@Autowired
	OrganisationRepository orgaDao;

	private final long ID = 48L;

	@Test
	@Sql(statements = "INSERT INTO organisation (id, nom, description, lien_web) VALUES (" + ID
			+ ", 'nomOrga', 'descOrga', 'lienWeb')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM organisation WHERE id = " + ID, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdateDelete_shouldUpdateEntity() {
		orgaDao.updateDelete(ID);

		Organisation response = orgaDao.findById(ID).get();
		assertTrue(response.isDeleted());
	}

	@Test
	void testUpdateDeleteInvalidIds_shouldNotThrowException() {
		assertThat(orgaDao).isNotNull();
		orgaDao.updateDelete(null);
		orgaDao.updateDelete(-1L);
	}

}
