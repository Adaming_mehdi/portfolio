package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Comment;

@SpringBootTest
class CommentRepositoryTests {

	@Autowired
	CommentRepository repo;

	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

	private final static long idEntity = 1L;

	@BeforeEach
	void beforeTouteMethode() {
		System.err.println("-------------------------Before each-------------------------");
	}

	@AfterAll
	@Sql(statements = { "DELETE FROM comment" })
	static void afterTouteMethode() {
		System.err.println("-------------------------After all-------------------------");
	}

	@Test
	@Sql(statements = { "DELETE FROM comment" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testCreatingComment_shouldHaveCreatedDateNotNull() {
		Comment comment = new Comment();
		comment.setContentComment("created");

		Comment entity = repo.save(comment);

		assertThat(entity).isNotNull();
		assertThat(entity.getId()).isNotNull();
		assertThat(entity.getDateCreation()).isNotNull();
		assertThat(entity.isDeleted()).isFalse();
		assertThat(entity.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());
	}

	@Test
	@Sql(statements = { "DELETE FROM comment" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdatingCommentById_shouldNotUpdateCreatedDate() {
		Comment comment = new Comment();
		comment.setContentComment("created");

		Comment entity = repo.save(comment);

		assertThat(entity).isNotNull().hasFieldOrPropertyWithValue("contentComment", comment.getContentComment())
				.hasFieldOrPropertyWithValue("deleted", false);
		assertThat(entity.getId()).isNotNull().isNotNegative();
		assertThat(entity.getDateCreation()).isNotNull();
		assertThat(entity.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());

		String date = entity.getDateCreation().toString();

// update field

		entity.setContentComment("updated");
		entity.setDeleted(true);

		Comment entity2 = repo.save(entity);

		assertThat(entity2.getId()).isNotNull().isEqualTo(entity.getId());
		assertThat(entity2).isNotNull().hasFieldOrPropertyWithValue("contentComment", entity.getContentComment())
				.hasFieldOrPropertyWithValue("deleted", true);
		assertThat(entity2.getDateCreation()).isNotNull();
		assertThat(entity2.getDateCreation()).asString().isEqualTo(date);
	}

	@Test
	@Sql(statements = {
			"INSERT INTO comment (id, content_comment, portfolio_id, recruter_id) VALUES (1, 'test1', null, null)",
			"INSERT INTO comment (id, content_comment, portfolio_id, recruter_id) VALUES (2, 'NoTest', null, null)",
			"INSERT INTO comment (id, content_comment, portfolio_id, recruter_id) VALUES (3, 'trick', null, null)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM comment" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testContentCommentListValidEntity_shouldShowOnlyCommentsContainingSearchEvenWithUpperOrLowerCase() {

		List<Comment> listRepo = repo.findByContentCommentContainingIgnoreCase("test");

		assertThat(listRepo).isNotEmpty().isNotNull().size().isEqualTo(2);
		assertThat(listRepo.get(0).getId()).isEqualTo(1L);
		assertThat(listRepo.get(0).getContentComment()).contains("test");
		assertThat(listRepo.get(1).getId()).isEqualTo(2L);
		assertThat(listRepo.get(1).getContentComment()).containsIgnoringCase("test");
	}

	@Test
	@Sql(statements = { "INSERT INTO comment (id, content_comment, portfolio_id, recruter_id) VALUES (" + idEntity
			+ ", 'content', null, null)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"DELETE FROM comment WHERE id = " + idEntity }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteValidEntity_shouldUpdateDeletedColumnOfGivenClass() {

		repo.fakeDelete(idEntity);

		Comment response = repo.findById(idEntity).get();

		assertThat(response.isDeleted()).isTrue();
	}

	@Test
	void testFakeDeleteByNegativeId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(-2L);
	}

	@Test
	void testFakeDeleteByZeroId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(0L);
	}

	@Test
	void testFakeDeleteByNullId_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(null);
	}

	@Test
	void testFakeDeleteByNotExistingEntity_shouldNotThrowException() {
		assertThat(repo).isNotNull();
		repo.fakeDelete(1L);
	}

}
