package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;


import com.fr.inti.entity.Interets;

@SpringBootTest
class InteretsRepositoryTests {

	@Autowired
	private InteretsRepository repo;
	
	private final long idTest = 12L;
		
	@Test
	@Sql(statements = "INSERT INTO interets (id, description, deleted) VALUES (" + idTest + ", 'decription4Test', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM interets WHERE id = " + idTest, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testFakeDeleteByValidId_shouldSetDeletedToTrue() {
		repo.fakeDelete(12L);
		Interets response = repo.findById(12L).get();
		assertThat(response.isDeleted()).isTrue();
		}
	
	
}
