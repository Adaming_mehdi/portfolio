package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;


import com.fr.inti.entity.Message;


@SpringBootTest
public class MessageRepositoryTest {


	@Autowired
	MessageRepository repo;

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

	//private final long idMessage = 1L;

	@BeforeEach
	void beforeTouteMethode() {
		System.err.println("-------------------------Before each-------------------------");
	}

	@AfterAll
	@Sql(statements = { "DELETE FROM message" })
	static void afterTouteMethode() {
		System.err.println("-------------------------After each-------------------------");
	}
	
	@Test
	@Sql(statements = { "DELETE FROM message" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testCreatingMessage_shouldHaveCreatedDateNotNull() {
		Message message = new Message();
		message.setContentMessage("created");

		Message msg = repo.save(message);

		assertThat(msg).isNotNull();
		assertThat(msg.getId()).isNotNull();
		assertThat(msg.getDateCreation()).isNotNull();
		assertThat(msg.isDeleted()).isFalse();
		assertThat(msg.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());
	}
	
	@Test
	@Sql(statements = { "DELETE FROM message" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testUpdatingMessage_ById_shouldNotUpdateCreatedDate() {
		Message message = new Message();
		message.setContentMessage("created");

		Message msg = repo.save(message);

		assertThat(msg.getId()).isNotNull().isNotNegative();
		assertThat(msg.getDateCreation()).isNotNull();
		assertThat(msg.getDateCreation()).asString().startsWith(LocalDateTime.now().format(formatter).toString());

		String date = msg.getDateCreation().toString();

		msg.setContentMessage("updated");
		msg.setDeleted(true);

		Message msg2 = repo.save(msg);

		assertThat(msg2.getId()).isNotNull().isEqualTo(msg.getId());
		assertThat(msg2).isNotNull().hasFieldOrPropertyWithValue("contentMessage", msg.getContentMessage());
		assertThat(msg2).hasFieldOrPropertyWithValue("deleted", true);
		assertThat(msg2.getDateCreation()).isNotNull();
		assertThat(msg2.getDateCreation()).asString().isEqualTo(date);
	
	}

	
/*
 * 
 */
	
	
	void testFakeDeleteByNegativeId_shouldNotThrowException() {
		repo.fakeDelete(-2L);
	}

	void testFakeDeleteByZeroId_shouldNotThrowException() {
		repo.fakeDelete(0L);
	}

	void testFakeDeleteByNullId_shouldNotThrowException() {
		repo.fakeDelete(null);
	}

	void testFakeDeleteByNotExistingEntity_shouldNotThrowException() {
		repo.fakeDelete(1L);
	}
	
}
