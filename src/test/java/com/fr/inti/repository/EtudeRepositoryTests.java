package com.fr.inti.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.inti.entity.Etude;

@SpringBootTest
public class EtudeRepositoryTests {
	
	@Autowired
	EtudeRepository etudeDao;
	
	private final long ID = 48L;
	private final long ID2 = 54L;
	private final long ID_ORGA = 2L;

	@Test
	@Sql(statements = {"INSERT INTO etude (id, nom, description) VALUES (" + ID + ", 'nomEtude', 'descEtude')", "INSERT INTO etude (id, nom, description) VALUES (" + ID2 + ", 'nomEtude2', 'descEtude2')"}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"DELETE FROM etude WHERE id = " + ID, "DELETE FROM etude WHERE id = " + ID2}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateDelete_shouldUpdateOneentity() {
		
		etudeDao.updateDelete(ID);
		
		Etude response = etudeDao.findById(ID).get();
		Etude response2 = etudeDao.findById(ID2).get();
		assertTrue(response.isDeleted());
		assertFalse(response2.isDeleted());
	}
	
	@Test
	@Sql(statements = "INSERT INTO etude (id, nom, description) VALUES (" + ID + ", 'nomEtude', 'descEtude')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM etude WHERE id = " + ID, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateDelete() {
		
		etudeDao.updateDelete(2L);
		
		Etude response = etudeDao.findById(ID).get();
		assertFalse(response.isDeleted());
	}
	
	@Test
	@Sql(statements = {"INSERT INTO organisation (id, nom, description, lien_web) VALUES (" + ID_ORGA + ", 'nomOrga', 'descOrga', 'lienWeb')", "INSERT INTO etude (id, nom, description, id_organisation) VALUES (" + ID + ", 'nomEtude', 'descEtude', " + ID_ORGA + ")", "INSERT INTO etude (id, nom, description) VALUES (" + ID2 + ", 'nomEtude2', 'descEtude2')"}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"DELETE FROM etude WHERE id = " + ID, "DELETE FROM etude WHERE id = " + ID2, "DELETE FROM organisation WHERE id = " + ID_ORGA}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFindEtudeByOrganisation() {
		
		List<Etude> etudeList = etudeDao.findEtudeByOrganisation(ID_ORGA);
		
		assertThat(etudeList).isNotEmpty();
		assertThat(etudeList).hasSize(1);
		assertEquals(etudeList.get(0).getId(), ID);
		assertEquals(etudeList.get(0).getOrganisation().getId(), ID_ORGA);
		
	}
	
	@Test
	public void testUpdateDeleteInvalidIds_shouldNotThrowException() {
		
		etudeDao.updateDelete(null);
		etudeDao.updateDelete(-1L);
		
	}

}
