package com.fr.inti;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PortfolioApplicationTests {

	@Test
	void contextLoads() {
		assertThat(PortfolioApplicationTests.class).isNotNull();
	}

}
