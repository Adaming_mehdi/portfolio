package com.fr.inti.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.FavorisCreateDTO;
import com.fr.inti.entity.Favoris;
import com.fr.inti.repository.FavorisRepository;

@SpringBootTest
@AutoConfigureMockMvc

 class FavorisControllerTest {
	@Autowired
	MockMvc mvc;
	
	@MockBean
	FavorisRepository dao;
		
	@Autowired 
	ObjectMapper mapper;
	List<Favoris> favList;
	
	@BeforeEach
	void init() {
		this.favList = new ArrayList<>();
		this.favList.add(new Favoris(15L,"",null,null));
		this.favList.add(new Favoris (16L,"",null,null));
		this.favList.add(new Favoris(17L,"",null,null));
	}
	
	
	@Test
	 void testFindAll_wouldReturnStatus200() throws Exception {
		given(dao.findAll()).willReturn(favList);
		
		String jsonResponse = mvc.perform(get("/favoris/all"))
		.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		System.err.println("RESPONSE VALUE  ====> " + jsonResponse);
		
		mvc.perform(get("/favoris/all"))
		.andExpect(jsonPath("$.size()",is(favList.size())))
		.andExpect(jsonPath("$.[0].libelle", is(favList.get(0).getLibelle())))
		.andExpect(jsonPath("$.[1].libelle", is(favList.get(1).getLibelle())))
		.andExpect(jsonPath("$.[2].libelle", is(favList.get(2).getLibelle())));
		
	}
	@Test
	 void testFindById_wouldReturnStatus200() throws Exception {
		final Long idfav = 84L;
		final Favoris favoris = new Favoris (1L,"test",null,null);
		
		given(dao.findById(idfav)).willReturn(Optional.of(favoris));
		
		mvc.perform(get("/favoris/find/84"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.libelle", is(favoris.getLibelle())));
	}
	@Test
	 void testCreateFavoris_wouldReturnStatus200() throws Exception {
		FavorisCreateDTO dto = new FavorisCreateDTO();
		dto.setId(48L);
		dto.setLibelle("testestes");
		
		
		String jsonDto = mapper.writeValueAsString(dto);
		
		mvc.perform(post("/favoris").contentType(MediaType.APPLICATION_JSON).content(jsonDto)).andExpect(status().isOk());
	
	}
	@Test
	 void testUpdate_shouldReturnStatus200() throws Exception {
		FavorisCreateDTO dto = new FavorisCreateDTO();
		dto.setId(48L);
		dto.setLibelle("testestes");
		
		String jsonDto = mapper.writeValueAsString(dto);
		
		mvc.perform(put("/favoris").contentType(MediaType.APPLICATION_JSON).content(jsonDto)).andExpect(status().isOk());
	
	}

}
