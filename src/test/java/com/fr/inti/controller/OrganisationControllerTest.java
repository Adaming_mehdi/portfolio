package com.fr.inti.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.OrganisationDTO;
import com.fr.inti.entity.Organisation;
import com.fr.inti.repository.OrganisationRepository;

@WebMvcTest(OrganisationController.class)
class OrganisationControllerTest {

	@Autowired
	MockMvc mvc;

	@MockBean
	OrganisationRepository orgaDao;

	@Autowired
	ObjectMapper mapper;

	@Test
	void testFindAll_shouldReturnStatus200() throws Exception {

		List<Organisation> orgaList = new ArrayList<>();
		Organisation orga = new Organisation(48L, "nom", "lienWeb", "description", null, null);
		orgaList.add(orga);
		when(orgaDao.findAll()).thenReturn(orgaList);

		String jsonResponse = mvc.perform(get("/organisation")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response : " + jsonResponse);

	}

	@Test
	void testFindById_shouldReturnStatus200() throws Exception {

		Organisation orga = new Organisation();
		orga.setId(48L);
		orga.setNom("nom");
		orga.setDescription("description");
		orga.setLienWeb("lien");
		when(orgaDao.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(orga));

		String jsonResponse = mvc.perform(get("/organisation/48")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		ObjectMapper mapper = new ObjectMapper();
		OrganisationDTO response = mapper.readValue(jsonResponse, OrganisationDTO.class);

		System.err.println("Response : " + response.toString());

	}

	@Test
	void testCreateOrga() throws Exception {
		OrganisationDTO dto = new OrganisationDTO();
		dto.setId(48L);
		dto.setNom("nom");
		dto.setDescription("description");
		dto.setLienWeb("lienWeb");

		String jsonDto = mapper.writeValueAsString(dto);

		mvc.perform(post("/organisation").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk());
	}

}
