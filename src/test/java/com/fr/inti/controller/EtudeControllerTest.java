package com.fr.inti.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.EtudeCreateDTO;
import com.fr.inti.dto.EtudeReadDTO;
import com.fr.inti.entity.Etude;
import com.fr.inti.repository.EtudeRepository;

@WebMvcTest(EtudeController.class)
public class EtudeControllerTest {
	
	@Autowired
	MockMvc mvc;
	
	@MockBean
	EtudeRepository orgaDao;
	
	@Autowired
	ObjectMapper mapper;
	
	@Test
	public void testFindAll_shouldReturnStatus200() throws Exception {
		
		List<Etude> etudeList = new ArrayList<>();
		Etude etude = new Etude(48L, "nom", "description", LocalDate.now(), LocalDate.now(), null, null, null, null);
		etudeList.add(etude);
		when(orgaDao.findAll()).thenReturn(etudeList);
		
		String jsonResponse = mvc.perform(
				get("/etude")
				).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		List<EtudeReadDTO> responseList = mapper.readValue(jsonResponse, List.class);
		
		System.err.println("Response : " + responseList.toString());
		assertThat(responseList).hasSize(1);
		
	}
	
	@Test
	public void testFindById_shouldReturnStatus200() throws Exception {
		
		Etude etude = new Etude(48L, "nom", "description", LocalDate.now(), LocalDate.now(), null, null, null, null);
		when(orgaDao.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(etude));
		
		String jsonResponse = mvc.perform(
				get("/etude/48")
				).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		ObjectMapper mapper = new ObjectMapper();
		EtudeReadDTO response = mapper.readValue(jsonResponse, EtudeReadDTO.class);
		
		System.err.println("Response : " + response.toString());
		
	}
	
	@Test
	public void testCreateOrga() throws Exception{
		EtudeCreateDTO dto = new EtudeCreateDTO();
		dto.setNom("nom");
		dto.setDescription("description");
		dto.setDateDebut("2020-01-01");
		dto.setDateFin("2020-01-01");
		
		String jsonDto = mapper.writeValueAsString(dto);
		
		mvc.perform(post("/etude").contentType(MediaType.APPLICATION_JSON).content(jsonDto)).andExpect(status().isOk());
	}

}
