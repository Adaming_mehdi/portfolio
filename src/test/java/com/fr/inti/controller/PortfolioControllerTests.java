package com.fr.inti.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.PortfolioUpdateDTO;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.entity.User;
import com.fr.inti.repository.PortfolioRepository;

@WebMvcTest(PortfolioController.class)
class PortfolioControllerTests {

	@Autowired
	MockMvc mvc;

	@MockBean
	PortfolioRepository dao;

	@Autowired
	ObjectMapper mapper;

	@BeforeEach
	void setPortfolio() {
		User u = new User();

		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(u);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		Portfolio dto2 = new Portfolio();
		dto1.setId(2L);
		dto1.setVisible(false);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(u);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		List<Portfolio> l = new ArrayList<>();
		l.add(dto1);
		l.add(dto2);
	}

// READ

	@Test
	void testFindById_shouldReturnStatus200() throws Exception {
// Préparer une requête HTTP (url, http-method:get, url-param, body-param, content-type, response-content-type:appli json, response-status:200)
// & Envoyer la requête & Récupérer la réponse
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());
		when(dao.findById(dto1.getId())).thenReturn(Optional.of(dto1));

		String jsonResponse = mvc.perform(get("/portfolio/" + dto1.getId())).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFindByIdNull_shouldReturnStatus400() throws Exception {
		String response = mvc.perform(get("/portfolio/null")).andExpect(status().isBadRequest()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdEqualToZero_shouldReturnStatus200() throws Exception {
		String response = mvc.perform(get("/portfolio/0")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdNegative_shouldReturnStatus200() throws Exception {
		String response = mvc.perform(get("/portfolio/-1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdNotPresent_shouldReturnStatus200() throws Exception {
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		when(dao.findById(dto1.getId())).thenReturn(Optional.of(dto1));

		String jsonResponse = mvc.perform(get("/portfolio/2")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// READ ALL

	@Test
	void testFindAll_shouldReturnStatus200() throws Exception {
		when(dao.findAll()).thenReturn(null);

		String jsonResponse = mvc.perform(get("/portfolio")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// READ ALL BY VISIBLE TRUE

	@Test
	void testFindByVisibleTrue_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(get("/portfolio/visible")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// CREATE

	@Test
	void testCreate_shouldReturnStatus200() throws Exception {
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		given(dao.save(any(Portfolio.class))).willAnswer((invocation) -> invocation.getArgument(0));

		String jsonResponse = mvc
				.perform(post("/portfolio").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// UPDATE

	@Test
	void testUpdate_shouldReturnStatus200() throws Exception {
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		given(dao.findById(dto1.getId())).willReturn(Optional.of(dto1));
		given(dao.save(any(Portfolio.class))).willAnswer((invocation) -> invocation.getArgument(0));

		String jsonResponse = mvc
				.perform(put("/portfolio").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testUpdateWithNullId_shouldReturnStatus200() throws Exception {
		PortfolioUpdateDTO dto = new PortfolioUpdateDTO();
		dto.setId(null);
		dto.setVisible(true);
		dto.setIdUser(null);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/portfolio").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value if null ==> created : " + response);
	}

	@Test
	void testUpdateWithNegativeId_shouldReturnStatus400() throws Exception {
		PortfolioUpdateDTO dto = new PortfolioUpdateDTO();
		dto.setId(-1L);
		dto.setVisible(true);
		dto.setIdUser(null);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/portfolio").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value if null ==> created : " + response);
	}

// DELETE

	@Test
	void testFakeDelete_shouldReturnStatus200() throws Exception {
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		String jsonResponse = mvc.perform(delete("/portfolio/1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNull_shouldReturnStatus400() throws Exception {
		String jsonResponse = mvc.perform(delete("/portfolio/null")).andExpect(status().isBadRequest()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdEqualToZero_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(delete("/portfolio/0")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNegative_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(delete("/portfolio/-1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNotPresent_shouldReturnStatus200() throws Exception {
		Portfolio dto1 = new Portfolio();
		dto1.setId(1L);
		dto1.setVisible(true);
		dto1.setInterets(null);
		dto1.setComments(null);
		dto1.setEtudes(null);
		dto1.setProjects(null);
		dto1.setUser(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		String jsonResponse = mvc
				.perform(delete("/portfolio/22").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

}
