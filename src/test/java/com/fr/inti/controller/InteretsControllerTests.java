package com.fr.inti.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.InteretsCreateDTO;

import com.fr.inti.entity.Interets;
import com.fr.inti.repository.InteretsRepository;

//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(InteretsController.class)
class InteretsControllerTests {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	ObjectMapper mapper;
	
	@MockBean
	InteretsRepository dao;
	
	
		
	@Test
	void testFindAll_shouldReturnStatus200() throws Exception {
		
		List<Interets> lstinter = new ArrayList<Interets>();
		
		when(dao.findAll()).thenReturn(lstinter);
		
		@SuppressWarnings("unused")
		String jsonResponse = mvc.perform(
				get("/Interets")
			//	.contentType(MediaType.APPLICATION_JSON) //pour préciser le content type si on envoie un body
			//	.content("{\"param\" : \"value\"}") //le body format json
				)
				.andExpect(
						status().isOk()
						)
				.andReturn()
				.getResponse()
				.getContentAsString();
		

		
	/*	List<InteretsReadDTO> list = mapper.readValue(jsonResponse, mapper.getTypeFactory().constructCollectionType(List.class, InteretsReadDTO.class));
		
		System.err.println("REPONSE VALUE : " + list);*/
		
		}
	
	@Test
	void testCreateInteretsNullDescription_shouldReturnStatus400() throws Exception {
		
		InteretsCreateDTO dto = new InteretsCreateDTO();
		dto.setDescription(null);
		dto.setIdPhoto(1L);
		dto.setIdPortfolio(1L);
		
		String jsonDto = mapper.writeValueAsString(dto);
		
		mvc.perform(post("/Interets").contentType(MediaType.APPLICATION_JSON).content(jsonDto)).andExpect(
				status().isBadRequest()
				);
		
		}
	
	
}
