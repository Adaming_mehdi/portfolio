package com.fr.inti.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.UserCreateDTO;
import com.fr.inti.entity.User;
import com.fr.inti.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.given;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(controllers = UserController.class)
@ActiveProfiles("test")
public class UserControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserRepository dao;

    @Autowired
    private ObjectMapper objectMapper;

    private List<User> userList;

    @BeforeEach
    void setUp() {
        this.userList = new ArrayList<>();
        this.userList.add(new User(1L,"test1","","","","",""));
        this.userList.add(new User(2L,"test2","","","","",""));
        this.userList.add(new User(2L,"test3","","","","",""));

    }
    @Test
    void shouldFetchAllUsers() throws Exception {

        given(dao.findAll()).willReturn(userList);

    this.mockMvc.perform(get("/user"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()", is(userList.size())))
            .andExpect(jsonPath("$.[0].nom", is(userList.get(0).getNom())))
            .andExpect(jsonPath("$.[1].nom", is(userList.get(1).getNom())))
            .andExpect(jsonPath("$.[2].nom", is(userList.get(2).getNom())));


    }

    @Test
    void shouldFetchOneUserById() throws Exception {
        final Long userId = 1L;
        final User user = new User(1L,"test4","ahmed","werss83","TEST","test","fr");

        given(dao.findById(userId)).willReturn(Optional.of(user));

        this.mockMvc.perform(get("/user/{id}", userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", is(user.getLogin())))
                .andExpect(jsonPath("$.prenom", is(user.getPrenom())));
    }


    @Test
    void shouldCreateNewUser() throws Exception {
        given(dao.save(any(User.class))).willAnswer((invocation) -> invocation.getArgument(0));

        User user = new User(1L,"test4","ahmed","werss83","TEST","test","fr");

        this.mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().is(200))
                .andExpect(content().string(" l'utilisateur : "+user.getNom()+" "+user.getPrenom()+" a été créé"))

        ;
    }

    @Test
    void shouldUpdateUser() throws Exception {
        Long userId = 1L;
        User user = new User(1L,"test4","ahmed","werss83","TEST","test","fr");
        given(dao.findById(userId)).willReturn(Optional.of(user));
        given(dao.save(any(User.class))).willAnswer((invocation) -> invocation.getArgument(0));

        this.mockMvc.perform(post("/user/update")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(content().string("L'utilisateur : "+user.getNom()+" "+user.getPrenom() +  " a été modifié"));
    }


    @Test
    void shouldDeleteUser() throws Exception {
        Long userId = 1L;
        User user = new User(1L,"test4","ahmed","werss83","TEST","test","fr");
        given(dao.findById(userId)).willReturn(Optional.of(user));
        doNothing().when(dao).deleteById(user.getId());

        this.mockMvc.perform(delete("/user/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string("L'utilisateur : " + user.getNom() + " " + user.getPrenom() + " a été supprimé"));
    }


    @Test
    void shouldReturnExplicitMessageWhenDeletingNonExistingUser() throws Exception {
        Long userId = 1L;
        given(dao.findById(userId)).willReturn(Optional.empty());

        this.mockMvc.perform(delete("/user/{id}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string(" L'utilisateur n'existe pas"));

    }

    @Test
    void shouldReturnExplicitMessageWhenUpdatingNonExistingUser() throws Exception {
        Long userId = 1L;
        given(dao.findById(userId)).willReturn(Optional.empty());
        User user = new User(1L,"test4","ahmed","werss83","TEST","test","fr");

        this.mockMvc.perform(post("/user/update", user)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(content().string(" L'utilisateur n'existe pas"));
    }


    @Test
    void shouldReturn400WhenCreateNewUserWithoutLogin() throws Exception {
        UserCreateDTO user = new UserCreateDTO();
        user.setNom("bendaoud");
        user.setPrenom("Ahmed");
        user.setLogin("");
        user.setPersonnalisation("test");
        user.setPassword("metek");
        this.mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest())

        ;
    }
}
