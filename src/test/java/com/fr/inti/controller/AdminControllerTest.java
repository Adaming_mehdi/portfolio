package com.fr.inti.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.AdminCreateDTO;
import com.fr.inti.entity.Admin;
import com.fr.inti.repository.AdminRepository;

@WebMvcTest(AdminController.class)
class AdminControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	ObjectMapper mapper;
	
	@MockBean
	AdminRepository dao;
	
	
	@Test
	void testFindAll_shouldReturnStatus200() throws Exception {
		
		List<Admin> lstadm = new ArrayList<Admin>();
		
		when(dao.findAll()).thenReturn(lstadm);
		
		@SuppressWarnings("unused")
		String jsonResponse = mvc.perform(
				get("/admin")
				)
				.andExpect(
						status().isOk()
						)
				.andReturn()
				.getResponse()
				.getContentAsString();
		
		}
	
	@Test
	void testCreateAdminNullLogin_shouldReturnStatus400() throws Exception {
		
		AdminCreateDTO dto = new AdminCreateDTO();
		dto.setLogin(null);
		dto.setNom("nom4test");
		dto.setPrenom("prenom4test");
		
		String jsonDto = mapper.writeValueAsString(dto);
		
		mvc.perform(post("/admin").contentType(MediaType.APPLICATION_JSON).content(jsonDto)).andExpect(
				status().isBadRequest()
				);
		
		}

}
