package com.fr.inti.controller;



import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fr.inti.dto.MessageCreateDTO;
import com.fr.inti.dto.MessageReadDTO;
import com.fr.inti.dto.MessageUpdateDTO;

import com.fr.inti.entity.Message;
import com.fr.inti.repository.MessageRepository;

//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(MessageController.class)
public class MessageControllerTest {
	 @Autowired
	    MockMvc mvc;
	 @Autowired
		ObjectMapper mapper;

	 @MockBean
	 MessageRepository dao;
	 
	 @Autowired
	 //private ObjectMapper objectMapper;
	 
	 //private List<Message> messageList;
	 
	 @BeforeEach
	    /*void setUp() {
		 this.messageList = new ArrayList<>();
		 this.messageList.add(new Message (1L , "test1", null, null));
		 this.messageList.add(new Message (2L , "test2", null, null));
		 this.messageList.add(new Message (3L , "test3", null, null)); 
	*/
	 void setMessage() {
		 Message dto1 = new Message();
			dto1.setId(1L);
			dto1.setContentMessage("test");
			dto1.setUser(null);
			dto1.setRecruter(null);
			dto1.setDeleted(false);
			dto1.setDateCreation(LocalDateTime.now());

			Message dto2 = new Message();
			dto2.setId(2L);
			dto2.setContentMessage("test");
			dto2.setUser(null);
			dto2.setRecruter(null);
			dto2.setDeleted(false);
			dto2.setDateCreation(LocalDateTime.now());

			List<Message> l = new ArrayList<>();
			l.add(dto1);
			l.add(dto2);
	 }
	 
	 @Test
	 void shouldFetchOneMessageByID() throws Exception{
		 //final Long messageId = 1L;
		 //final Message message = new Message(1L, "test1", null, null);
		 
		 this.mvc.perform(get("/message"))
		 .andExpect (status().isOk());
		 
	 }
	 

		@Test
		public void testFindById_shouldReturnStatus200() throws Exception {
	// Préparer une requête HTTP (url, http-method:get, url-param, body-param, content-type, response-content-type:appli json, response-status:200)
	// & Envoyer la requête & Récupérer la réponse
			Message dto1 = new Message();
			dto1.setId(1L);
			dto1.setContentMessage("test");
			dto1.setUser(null);
			dto1.setRecruter(null);
			dto1.setDeleted(false);
			dto1.setDateCreation(LocalDateTime.now());

			when(dao.findById(dto1.getId())).thenReturn(Optional.of(dto1));

			String jsonResponse = mvc.perform(get("/message/" + dto1.getId())).andExpect(status().isOk()).andReturn()
					.getResponse().getContentAsString();

			System.err.println("Response value : " + jsonResponse);
		}
		
		
	 
	// READ

	 
	 @Test
		public void testFindByIdNull_shouldReturnStatus400() throws Exception {
			MessageReadDTO dto = new MessageReadDTO();
			dto.setId(null);
			dto.setContentMessage("test");
			dto.setIdUser(null);
			dto.setIdRecruter(null);
			dto.setDeleted(false);
			dto.setDateCreation(LocalDateTime.now().toString());

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc
					.perform(get("/message/" + dto.getId()).contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value : " + response);
			
		}
	 


	 // CREATE
	
	 
	 @Test
		public void testCreateWithNullContent_shouldReturnStatus400() throws Exception {
			MessageCreateDTO dto = new MessageCreateDTO();
			dto.setContentMessage(null);
			dto.setIdUser(1L);
			dto.setIdRecruter(1L);

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc.perform(post("/message").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value : " + response);
		}
	 
	// UPDATE
	 @Test
		public void testUpdateWithNullId_shouldReturnStatus200() throws Exception {
			MessageUpdateDTO dto = new MessageUpdateDTO();
			dto.setId(null);
			dto.setContentMessage("test");
			dto.setIdUser(1L);
			dto.setIdRecruter(1L);

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc.perform(put("/message").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value if null ==> created : " + response);
		}
	 
	 @Test
		public void testUpdateWithNullContent_shouldReturnStatus400() throws Exception {
			MessageUpdateDTO dto = new MessageUpdateDTO();
			dto.setId(1L);
			dto.setContentMessage(null);
			dto.setIdUser(1L);
			dto.setIdRecruter(1L);

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc.perform(put("/message").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value : " + response);
		}
	 
	 @Test
		public void testUpdateWithNullIdUser_shouldReturnStatus200() throws Exception {
			MessageUpdateDTO dto = new MessageUpdateDTO();
			dto.setId(1L);
			dto.setContentMessage("test");
			dto.setIdUser(null);
			dto.setIdRecruter(1L);

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc.perform(put("/message").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value : " + response);
		}
	 
	 @Test
		public void testUpdateWithNullIdRecruter_shouldReturnStatus200() throws Exception {
			MessageUpdateDTO dto = new MessageUpdateDTO();
			dto.setId(1L);
			dto.setContentMessage("test");
			dto.setIdUser(1L);
			dto.setIdRecruter(null);

			String jsonDto = mapper.writeValueAsString(dto);

			String response = mvc.perform(put("/message").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
					.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

			System.err.println("Response value : " + response);
		}
	 
	// DELETE

		@Test
		public void testFakeDelete_shouldReturnStatus200() throws Exception {

			String jsonResponse = mvc.perform(delete("/message/1")).andExpect(status().isOk()).andReturn().getResponse()
					.getContentAsString();

			System.err.println("Response value : " + jsonResponse);
		}

		@Test
		public void testFakeDeleteIdNull_shouldReturnStatus400() throws Exception {

			String jsonResponse = mvc.perform(delete("/message/null")).andExpect(status().isBadRequest()).andReturn()
					.getResponse().getContentAsString();

			System.err.println("Response value : " + jsonResponse);
		}
		
	 
}
