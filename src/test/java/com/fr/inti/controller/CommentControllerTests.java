package com.fr.inti.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.inti.dto.CommentCreateDTO;
import com.fr.inti.dto.CommentUpdateDTO;
import com.fr.inti.entity.Comment;
import com.fr.inti.repository.CommentRepository;

@WebMvcTest(CommentController.class)
class CommentControllerTests {

	@Autowired
	MockMvc mvc;

	@MockBean
	CommentRepository dao;

	@Autowired
	ObjectMapper mapper;

// READ

	@Test
	void testFindById_shouldReturnStatus200() throws Exception {
// Préparer une requête HTTP (url, http-method:get, url-param, body-param, content-type, response-content-type:appli json, response-status:200)
// & Envoyer la requête & Récupérer la réponse
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		when(dao.findById(dto1.getId())).thenReturn(Optional.of(dto1));

		String jsonResponse = mvc.perform(get("/comment/" + dto1.getId())).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFindByIdNull_shouldReturnStatus400() throws Exception {
		String response = mvc.perform(get("/comment/null")).andExpect(status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdEqualToZero_shouldReturnStatus200() throws Exception {
		String response = mvc.perform(get("/comment/0")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdNegative_shouldReturnStatus200() throws Exception {
		String response = mvc.perform(get("/comment/-1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testFindByIdNotPresent_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		when(dao.findById(dto1.getId())).thenReturn(Optional.of(dto1));

		String jsonResponse = mvc.perform(get("/comment/2")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// READ ALL

	@Test
	void testFindAll_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		Comment dto2 = new Comment();
		dto2.setId(2L);
		dto2.setContentComment("test");
		dto2.setPortfolio(null);
		dto2.setRecruter(null);
		dto2.setDeleted(false);
		dto2.setDateCreation(LocalDateTime.now());

		List<Comment> l = new ArrayList<>();
		l.add(dto1);
		l.add(dto2);

		when(dao.findAll()).thenReturn(l);

		String jsonResponse = mvc.perform(get("/comment")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// READ ALL CONTENT CONTAINING STRING INJECTED

	@ParameterizedTest
	@ValueSource(strings = { "test", "tEs", "notpresent" })
	void testFindContent_shouldReturnStatus200(String arg) throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		Comment dto2 = new Comment();
		dto2.setId(2L);
		dto2.setContentComment("autre");
		dto2.setPortfolio(null);
		dto2.setRecruter(null);
		dto2.setDeleted(false);
		dto2.setDateCreation(LocalDateTime.now());

		List<Comment> l = new ArrayList<>();
		l.add(dto1);
		l.add(dto2);

		when(dao.findByContentCommentContainingIgnoreCase(arg)).thenReturn(l);

		String jsonResponse = mvc.perform(get("/comment/content/{contentComment}", arg)).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFindByContentNull_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(get("/comment/content/null")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFindByContentBlank_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(get("/comment/content/ ")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

// CREATE

	@Test
	void testCreate_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		given(dao.save(any(Comment.class))).willAnswer((invocation) -> invocation.getArgument(0));

		String jsonResponse = mvc
				.perform(post("/comment").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testCreateWithNullContent_shouldReturnStatus400() throws Exception {

		CommentCreateDTO dto = new CommentCreateDTO();
		dto.setContentComment(null);
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(post("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testCreateWithNullIdPortfolio_shouldReturnStatus200() throws Exception {

		CommentCreateDTO dto = new CommentCreateDTO();
		dto.setContentComment("test");
		dto.setIdPortfolio(null);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(post("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testCreateWithNullIdRecruter_shouldReturnStatus200() throws Exception {

		CommentCreateDTO dto = new CommentCreateDTO();
		dto.setContentComment("test");
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(null);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(post("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

// UPDATE

	@Test
	void testUpdate_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		given(dao.findById(dto1.getId())).willReturn(Optional.of(dto1));
		given(dao.save(any(Comment.class))).willAnswer((invocation) -> invocation.getArgument(0));

		String jsonResponse = mvc
				.perform(put("/comment").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testUpdateWithNullId_shouldReturnStatus200() throws Exception {
		CommentUpdateDTO dto = new CommentUpdateDTO();
		dto.setId(null);
		dto.setContentComment("test");
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value if null ==> created : " + response);
	}

	@Test
	void testUpdateWithNegativeId_shouldReturnStatus400() throws Exception {
		CommentUpdateDTO dto = new CommentUpdateDTO();
		dto.setId(-1L);
		dto.setContentComment("test");
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value if null ==> created : " + response);
	}

	@Test
	void testUpdateWithNullContent_shouldReturnStatus400() throws Exception {
		CommentUpdateDTO dto = new CommentUpdateDTO();
		dto.setId(1L);
		dto.setContentComment(null);
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testUpdateWithNullIdPortfolio_shouldReturnStatus200() throws Exception {
		CommentUpdateDTO dto = new CommentUpdateDTO();
		dto.setId(1L);
		dto.setContentComment("test");
		dto.setIdPortfolio(null);
		dto.setIdRecruter(1L);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

	@Test
	void testUpdateWithNullIdRecruter_shouldReturnStatus200() throws Exception {
		CommentUpdateDTO dto = new CommentUpdateDTO();
		dto.setId(1L);
		dto.setContentComment("test");
		dto.setIdPortfolio(1L);
		dto.setIdRecruter(null);

		String jsonDto = mapper.writeValueAsString(dto);

		String response = mvc.perform(put("/comment").contentType(MediaType.APPLICATION_JSON).content(jsonDto))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + response);
	}

// DELETE

	@Test
	void testFakeDelete_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		String jsonResponse = mvc.perform(delete("/comment/1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNull_shouldReturnStatus400() throws Exception {
		String jsonResponse = mvc.perform(delete("/comment/null")).andExpect(status().isBadRequest()).andReturn()
				.getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdEqualToZero_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(delete("/comment/0")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNegative_shouldReturnStatus200() throws Exception {
		String jsonResponse = mvc.perform(delete("/comment/-1")).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

	@Test
	void testFakeDeleteIdNotPresent_shouldReturnStatus200() throws Exception {
		Comment dto1 = new Comment();
		dto1.setId(1L);
		dto1.setContentComment("test");
		dto1.setPortfolio(null);
		dto1.setRecruter(null);
		dto1.setDeleted(false);
		dto1.setDateCreation(LocalDateTime.now());

		String jsonResponse = mvc
				.perform(delete("/comment/22").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto1)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.err.println("Response value : " + jsonResponse);
	}

}
