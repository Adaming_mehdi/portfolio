package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.PortfolioCreateDTO;
import com.fr.inti.dto.PortfolioReadDTO;
import com.fr.inti.dto.PortfolioUpdateDTO;
import com.fr.inti.dto.UserReadDTO;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.entity.User;

// @SpringBootTest // pas nécessaire pour les converters car ils n'appellent pas de @Bean ou d'autres classes (@Autowired)
class PortfolioConverterTests {

	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("'Créé le' yyyy-MM-dd 'à' HH:mm:ss");

// DTO TO ENTITY

// Valid Attributes

	@Test
	void testConvertValidReadDtoToEntity_shouldReturnValidEntity() {
		// Préparation des entrées (inputs)
		PortfolioReadDTO dto = new PortfolioReadDTO();
		UserReadDTO dtoUser = new UserReadDTO();
		dtoUser.setId(1L);

		dto.setId((long) 1);
		dto.setVisible(true);
		dto.setUser(dtoUser);
		dto.setDeleted(false);
		dto.setDateCreation(LocalDateTime.now().toString());

		// Invocation de l'application : les méthodes à tester
		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		// Vérification du résultat retourné par l'application
		// <==> Assertions.assertNotNull(portfolio);
		assertThat(portfolio).isNotNull().hasFieldOrPropertyWithValue("id", dto.getId())
				.hasFieldOrPropertyWithValue("visible", dto.isVisible());
		// <==> Assertions.assertEquals(dto.isVisible(), portfolio.getVisible());
		assertThat(portfolio.getUser().getId()).isEqualTo(dto.getUser().getId());
		assertThat(portfolio.isDeleted()).isEqualTo(dto.isDeleted());
		assertThat(portfolio.getDateCreation().toString()).hasToString(dto.getDateCreation());
	}

	@Test
	void testConvertValidCreateDtoToEntity_shouldReturnValidEntity() {
		PortfolioCreateDTO dto = new PortfolioCreateDTO();

		dto.setVisible(true);
		dto.setIdUser((long) 1);

		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		assertThat(portfolio).isNotNull().hasFieldOrPropertyWithValue("visible", dto.isVisible());
		assertThat(portfolio.getUser().getId()).isEqualTo(dto.getIdUser());
	}

	@Test
	void testConvertValidUpdateDtoToEntity_shouldReturnValidEntity() {
		PortfolioUpdateDTO dto = new PortfolioUpdateDTO();

		dto.setId((long) 1);
		dto.setVisible(true);
		dto.setIdUser((long) 1);

		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		assertThat(portfolio).isNotNull().hasFieldOrPropertyWithValue("id", dto.getId())
				.hasFieldOrPropertyWithValue("visible", dto.isVisible());
		assertThat(portfolio.getUser().getId()).isEqualTo(dto.getIdUser());
	}

// Null objects

	@Test
	void testConvertNullReadDtoToEntity_shouldReturnNull() {
		PortfolioReadDTO dto = null;
		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		assertThat(portfolio).isNull();
	}

	@Test
	void testConvertNullListDtoToEntity_shouldReturnNull() {
		List<Portfolio> portfolio = PortfolioConverter.convertToEntityList(null);

		assertThat(portfolio).isEmpty();
	}

	@Test
	void testConvertNullCreateDtoToEntity_shouldReturnNull() {
		PortfolioCreateDTO dto = null;
		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		assertThat(portfolio).isNull();
	}

	@Test
	void testConvertNullUpdateDtoToEntity_shouldReturnNull() {
		PortfolioUpdateDTO dto = null;
		Portfolio portfolio = PortfolioConverter.convertToEntity(dto);

		assertThat(portfolio).isNull();
	}

// ENTITY TO DTO --------------------------------------------------------------------------------------------

// Valid Attributes

	@Test
	void testConvertValidReadEntityToDto_shouldReturnValidEntity() {
		Portfolio portfolio = new Portfolio();
		User portfolioUser = new User();
		portfolioUser.setAvatarUrl("test");
		portfolioUser.setPersonnalisation("test");

		portfolio.setId((long) 1);
		portfolio.setVisible(true);
		portfolio.setUser(portfolioUser);
		portfolio.setDeleted(false);
		portfolio.setDateCreation(LocalDateTime.now());

		PortfolioReadDTO dto = PortfolioConverter.convertToReadDTO(portfolio);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("id", portfolio.getId())
				.hasFieldOrPropertyWithValue("visible", portfolio.getVisible());
		assertThat(dto.getUser().getId()).isEqualTo(portfolio.getUser().getId());
		assertThat(dto.isDeleted()).isEqualTo(portfolio.isDeleted());
		assertThat(dto.getDateCreation()).isEqualTo(portfolio.getDateCreation().format(formatter).toString());
	}

	@Test
	void testConvertValidCreateEntityToDto_shouldReturnValidEntity() {
		Portfolio portfolio = new Portfolio();
		User portfolioUser = new User();

		portfolio.setVisible(true);
		portfolio.setUser(portfolioUser);

		PortfolioCreateDTO dto = PortfolioConverter.convertToCreateDTO(portfolio);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("visible", portfolio.getVisible());
		assertThat(dto.getIdUser()).isEqualTo(portfolio.getUser().getId());
	}

	@Test
	void testConvertValidUpdateEntityToDto_shouldReturnValidEntity() {
		Portfolio portfolio = new Portfolio();
		User portfolioUser = new User();

		portfolio.setId((long) 1);
		portfolio.setVisible(true);
		portfolio.setUser(portfolioUser);

		PortfolioUpdateDTO dto = PortfolioConverter.convertToUpdateDTO(portfolio);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("id", portfolio.getId())
				.hasFieldOrPropertyWithValue("visible", portfolio.getVisible());
		assertThat(dto.getIdUser()).isEqualTo(portfolio.getUser().getId());
	}

// Null objects

	@Test
	void testConvertNullReadEntityToDto_shouldReturnNull() {
		PortfolioReadDTO dto = PortfolioConverter.convertToReadDTO(null);

		assertThat(dto).isNull();
	}

	@Test
	void testConvertNullListEntityToDto_shouldReturnNull() {
		List<PortfolioReadDTO> dto = PortfolioConverter.convertToDtoList(null);

		assertThat(dto).isEmpty();
	}

	@Test
	void testConvertNullCreateEntityToDto_shouldReturnNull() {
		PortfolioCreateDTO dto = PortfolioConverter.convertToCreateDTO(null);

		assertThat(dto).isNull();
	}

	@Test
	void testConvertNullUpdateEntityToDto_shouldReturnNull() {
		PortfolioUpdateDTO dto = PortfolioConverter.convertToUpdateDTO(null);

		assertThat(dto).isNull();
	}

}
