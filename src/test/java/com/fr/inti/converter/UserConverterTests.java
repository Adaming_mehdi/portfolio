package com.fr.inti.converter;

import com.fr.inti.dto.UserCreateDTO;
import com.fr.inti.dto.UserReadDTO;
import com.fr.inti.dto.UserUpdateDTO;
import com.fr.inti.entity.User;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserConverterTests {

    @Test
    @DisplayName("Test convert valid DTO to Entity")
    void testCreate_shoudReturnValidEntity() {
        UserCreateDTO dto = new UserCreateDTO();

        dto.setNom("Ben Daoud");
        dto.setPrenom("Ahmed");
        dto.setAvatarUrl("4test");
        dto.setLogin("werss83");
        dto.setPassword("4test");
        dto.setPersonnalisation("4test");

        User entity = UserConverter.convertToCreateEntity(dto);

        assertNotNull(entity);
        assertEquals(dto.getNom(),entity.getNom());
        assertEquals(dto.getPrenom(),entity.getPrenom());
        assertEquals(dto.getAvatarUrl(),entity.getAvatarUrl());
        assertEquals(dto.getLogin(),entity.getLogin());
        assertEquals(dto.getPassword(),entity.getPassword());
        assertEquals(dto.getPersonnalisation(),entity.getPersonnalisation());
        assertThat(entity.isDeleted()).isFalse();
        assertThat(entity.getDateCreation()).isNotNull();
    }

    @Test
    @DisplayName("Test convert valid UpdateDTO to Entity")
    void testUpdate_shoudReturnValidEntity() {
        UserUpdateDTO dto = new UserUpdateDTO();

        dto.setId(1L);
        dto.setNom("Ben Daoud");
        dto.setPrenom("Ahmed");
        dto.setAvatarUrl("4test");
        dto.setLogin("werss83");
        dto.setPassword("4test");
        dto.setPersonnalisation("4test");

        User entity = UserConverter.convertToUpdateEntity(dto);

        assertNotNull(entity);
        assertThat(entity.getId()).isNotNull();
        assertEquals(dto.getNom(),entity.getNom());
        assertEquals(dto.getPrenom(),entity.getPrenom());
        assertEquals(dto.getAvatarUrl(),entity.getAvatarUrl());
        assertEquals(dto.getLogin(),entity.getLogin());
        assertEquals(dto.getPassword(),entity.getPassword());
        assertEquals(dto.getPersonnalisation(),entity.getPersonnalisation());
        assertThat(entity.isDeleted()).isFalse();
        assertThat(entity.getDateCreation()).isNotNull();
    }


    @Test
    @DisplayName("Test convert valid Entity to DTO")
    void testReadDTO_shoudReturnValidDTO() {
        User entity = new User();

        entity.setNom("Ben Daoud");
        entity.setPrenom("Ahmed");
        entity.setAvatarUrl("4test");
        entity.setLogin("werss83");
        entity.setPassword("4test");
        entity.setPersonnalisation("4test");
        entity.setDateCreation(LocalDateTime.now());

        UserReadDTO dto = UserConverter.convertToReadDTO(entity);

        assertNotNull(entity);
        assertEquals(entity.getNom(),dto.getNom());
        assertEquals(entity.getPrenom(),dto.getPrenom());
        assertEquals(entity.getAvatarUrl(),dto.getAvatarUrl());
        assertEquals(entity.getLogin(),entity.getLogin());
        assertEquals(entity.getPassword(),dto.getPassword());
        assertEquals(entity.getPersonnalisation(),dto.getPersonnalisation());
        assertThat(dto.isDeleted()).isFalse();
        assertThat(dto.getDateCreation()).isNotNull();
    }

    @Test
    @DisplayName("Test convert valid Entity list to DTO list")
    void testListDTO_shoudReturnValidListDTO() {
        User user = new User();
        user.setNom("Ben Daoud");
        user.setPrenom("Ahmed");
        user.setAvatarUrl("4test");
        user.setLogin("werss83");
        user.setPassword("4test");
        user.setPersonnalisation("4test");

        User user2 = new User();
        user2.setNom("lastNameUser2");
        user2.setPrenom("nameUser2");
        user2.setAvatarUrl("4test");
        user2.setLogin("LoginUser2");
        user2.setPassword("4test");
        user2.setPersonnalisation("4test");

        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        List<UserReadDTO> usersDTO = UserConverter.convertToDtoList(users);
        assertThat(usersDTO).asList().size().isEqualTo(2);
        assertThat(usersDTO.get(0)).hasFieldOrPropertyWithValue("prenom",user.getPrenom());
        assertThat(usersDTO.get(1)).hasFieldOrPropertyWithValue("login",user2.getLogin());

    }

    @Test
    @DisplayName("check that the injection of a empty DTO list does not generate an Exception")
    void testEmptyListDTO_dontShouldGenerateException() {
        List<User> users = new ArrayList<>();
        List<UserReadDTO> usersDTO = UserConverter.convertToDtoList(users);
        //assertThat(usersDTO).isNull();
    }
}
