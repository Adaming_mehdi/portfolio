package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.CommentCreateDTO;
import com.fr.inti.dto.CommentReadDTO;
import com.fr.inti.dto.CommentUpdateDTO;
import com.fr.inti.dto.PortfolioReadDTO;
import com.fr.inti.dto.RecruterReadDTO;
import com.fr.inti.entity.Comment;
import com.fr.inti.entity.Portfolio;
import com.fr.inti.entity.Recruter;

class CommentConverterTests {

	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("'Créé le' yyyy-MM-dd 'à' HH:mm:ss");

// DTO TO ENTITY

// Valid Attributes

	@Test
	void testConvertValidReadDtoToEntity_shouldReturnValidEntity() {
		CommentReadDTO dto = new CommentReadDTO();
		PortfolioReadDTO dtoPortfolio = new PortfolioReadDTO();
		dtoPortfolio.setId(1L);
		RecruterReadDTO dtoRecruter = new RecruterReadDTO();
		dtoRecruter.setId(1L);

		dto.setId((long) 1);
		dto.setContentComment("content");
		dto.setPortfolio(dtoPortfolio);
		dto.setRecruter(dtoRecruter);
		dto.setDeleted(false);
		dto.setDateCreation(LocalDateTime.now().toString());

		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNotNull().hasFieldOrPropertyWithValue("id", dto.getId())
				.hasFieldOrPropertyWithValue("contentComment", dto.getContentComment());
		assertThat(comment.getPortfolio().getId()).isEqualTo(dto.getPortfolio().getId());
		assertThat(comment.getRecruter().getId()).isEqualTo(dto.getRecruter().getId());
		assertThat(comment.isDeleted()).isEqualTo(dto.isDeleted());
		assertThat(comment.getDateCreation().toString()).hasToString(dto.getDateCreation());
	}

	@Test
	void testConvertValidCreateDtoToEntity_shouldReturnValidEntity() {
		CommentCreateDTO dto = new CommentCreateDTO();

		dto.setContentComment("content");
		dto.setIdPortfolio((long) 1);
		dto.setIdRecruter((long) 1);

		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNotNull().hasFieldOrPropertyWithValue("contentComment", dto.getContentComment());
		assertThat(comment.getPortfolio().getId()).isEqualTo(dto.getIdPortfolio());
		assertThat(comment.getRecruter().getId()).isEqualTo(dto.getIdRecruter());
	}

	@Test
	void testConvertValidUpdateDtoToEntity_shouldReturnValidEntity() {
		CommentUpdateDTO dto = new CommentUpdateDTO();

		dto.setId((long) 1);
		dto.setContentComment("content");
		dto.setIdPortfolio((long) 1);
		dto.setIdRecruter((long) 1);

		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNotNull().hasFieldOrPropertyWithValue("id", dto.getId())
				.hasFieldOrPropertyWithValue("contentComment", dto.getContentComment());
		assertThat(comment.getPortfolio().getId()).isEqualTo(dto.getIdPortfolio());
		assertThat(comment.getRecruter().getId()).isEqualTo(dto.getIdRecruter());
	}

// Null objects

	@Test
	void testConvertNullReadDtoToEntity_shouldReturnNull() {
		CommentReadDTO dto = null;
		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNull();
	}

	@Test
	void testConvertNullListDtoToEntity_shouldReturnNull() {
		List<Comment> comment = CommentConverter.convertToEntityList(null);

		assertThat(comment).isEmpty();
	}

	@Test
	void testConvertNullCreateDtoToEntity_shouldReturnNull() {
		CommentCreateDTO dto = null;
		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNull();
	}

	@Test
	void testConvertNullUpdateDtoToEntity_shouldReturnNull() {
		CommentUpdateDTO dto = null;
		Comment comment = CommentConverter.convertToEntity(dto);

		assertThat(comment).isNull();
	}

// ENTITY TO DTO --------------------------------------------------------------------------------------------

// Valid Attributes

	@Test
	void testConvertValidReadEntityToDto_shouldReturnValidEntity() {
		Comment comment = new Comment();
		Portfolio commentPortfolio = new Portfolio();
		commentPortfolio.setVisible(true);
		commentPortfolio.setDateCreation(LocalDateTime.now());
		Recruter commentRecruter = new Recruter();

		comment.setId((long) 1);
		comment.setContentComment("content");
		comment.setPortfolio(commentPortfolio);
		comment.setRecruter(commentRecruter);
		comment.setDeleted(false);
		comment.setDateCreation(LocalDateTime.now());

		CommentReadDTO dto = CommentConverter.convertToReadDTO(comment);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("id", comment.getId())
				.hasFieldOrPropertyWithValue("contentComment", comment.getContentComment());
		assertThat(dto.getPortfolio().getId()).isEqualTo(comment.getPortfolio().getId());
		assertThat(dto.getRecruter().getId()).isEqualTo(comment.getRecruter().getId());
		assertThat(dto.isDeleted()).isEqualTo(comment.isDeleted());
		assertThat(dto.getDateCreation()).isEqualTo(comment.getDateCreation().format(formatter).toString());
	}

	@Test
	void testConvertValidCreateEntityToDto_shouldReturnValidEntity() {
		Comment comment = new Comment();
		Portfolio commentPortfolio = new Portfolio();
		Recruter commentRecruter = new Recruter();

		comment.setContentComment("content");
		comment.setPortfolio(commentPortfolio);
		comment.setRecruter(commentRecruter);

		CommentCreateDTO dto = CommentConverter.convertToCreateDTO(comment);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("contentComment", comment.getContentComment());
		assertThat(dto.getIdPortfolio()).isEqualTo(comment.getPortfolio().getId());
		assertThat(dto.getIdRecruter()).isEqualTo(comment.getRecruter().getId());
	}

	@Test
	void testConvertValidUpdateEntityToDto_shouldReturnValidEntity() {
		Comment comment = new Comment();
		Portfolio commentPortfolio = new Portfolio();
		Recruter commentRecruter = new Recruter();

		comment.setId((long) 1);
		comment.setContentComment("content");
		comment.setPortfolio(commentPortfolio);
		comment.setRecruter(commentRecruter);

		CommentUpdateDTO dto = CommentConverter.convertToUpdateDTO(comment);

		assertThat(dto).isNotNull().hasFieldOrPropertyWithValue("id", comment.getId())
				.hasFieldOrPropertyWithValue("contentComment", comment.getContentComment());
		assertThat(dto.getIdPortfolio()).isEqualTo(comment.getPortfolio().getId());
		assertThat(dto.getIdRecruter()).isEqualTo(comment.getRecruter().getId());
	}

// Null objects

	@Test
	void testConvertNullReadEntityToDto_shouldReturnNull() {
		CommentReadDTO dto = CommentConverter.convertToReadDTO(null);

		assertThat(dto).isNull();
	}

	@Test
	void testConvertNullListEntityToDto_shouldReturnNull() {
		List<CommentReadDTO> dto = CommentConverter.convertToDtoList(null);

		assertThat(dto).isEmpty();
	}

	@Test
	void testConvertNullCreateEntityToDto_shouldReturnNull() {
		CommentCreateDTO dto = CommentConverter.convertToCreateDTO(null);

		assertThat(dto).isNull();
	}

	@Test
	void testConvertNullUpdateEntityToDto_shouldReturnNull() {
		CommentUpdateDTO dto = CommentConverter.convertToUpdateDTO(null);

		assertThat(dto).isNull();
	}

}
