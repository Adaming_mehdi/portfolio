package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.InteretsCreateDTO;
import com.fr.inti.dto.InteretsReadDTO;
import com.fr.inti.entity.Interets;
import com.fr.inti.entity.Photo;
import com.fr.inti.entity.Portfolio;


//@SpringBootTest
class InteretsConverterTests {

	
	@Test
	void testConvertValidDtoToEntity_shouldReturnValidEntity(){
		InteretsCreateDTO dto = new InteretsCreateDTO();
		
		dto.setDescription("description4Test");
		dto.setIdPortfolio(1L);
		dto.setIdPhoto(1L);
	
		Interets entity = InteretsConverter.convertToEntity(dto);
		
		assertThat(entity).hasFieldOrPropertyWithValue("description", "description4Test");
		assertThat(entity.getPortfolio().getId()).isEqualTo(1L);
		assertThat(entity.getPhoto().getId()).isEqualTo(1L);
	
	}
	
	@Test
	void testConvertNullIdPortfolioDtoToEntity_shouldReturnValidEntity(){
		InteretsCreateDTO dto = new InteretsCreateDTO();
		
		dto.setDescription("description4Test");
		dto.setIdPortfolio(null);
		dto.setIdPhoto(1L);
	
		Interets entity = InteretsConverter.convertToEntity(dto);
		
		assertThat(entity).hasFieldOrPropertyWithValue("description", "description4Test");
		assertThat(entity.getPortfolio()).isNull();
		assertThat(entity.getPhoto().getId()).isEqualTo(1L);
	
	}
	
	@Test
	void testConvertNullIdPhotoDtoToEntity_shouldReturnValidEntity(){
		InteretsCreateDTO dto = new InteretsCreateDTO();
		
		dto.setDescription("description4Test");
		dto.setIdPortfolio(1L);
		dto.setIdPhoto(null);
	
		Interets entity = InteretsConverter.convertToEntity(dto);
		
		assertThat(entity).hasFieldOrPropertyWithValue("description", "description4Test");
		assertThat(entity.getPortfolio().getId()).isEqualTo(1L);
		assertThat(entity.getPhoto()).isNull();
	
	}
	
	@Test
	void testConvertNullDtoToEntity_shouldReturnNull() {
		
		Interets entity = InteretsConverter.convertToEntity(null);
		
		assertThat(entity).isNull();
		
	}
	
	
	@Test
	void testConvertValidEntityToDto_shouldReturnValidReadDto(){
		Interets entity = new Interets();
		Photo photo = new Photo();
		photo.setId(1L);
		Portfolio port = new Portfolio();
		port.setId(1L);
		
		entity.setDateCreation(LocalDateTime.now());
		entity.setId(1L);
		entity.setDeleted(true);
		entity.setDescription("description4Test");
		entity.setPhoto(photo);
		entity.setPortfolio(port);
		
		InteretsReadDTO dto = InteretsConverter.convertToDTO(entity);
		
		assertThat(dto.getDateCrea()).startsWith(LocalDate.now().toString());
		assertThat(dto).hasFieldOrPropertyWithValue("id", 1L).hasFieldOrPropertyWithValue("description", "description4Test").hasFieldOrPropertyWithValue("idPhoto",1L);
		
	
	}
	
	@Test
	void testConvertNullPhotoToDto_shouldReturnValidReadDto(){
		Interets entity = new Interets();

		
		entity.setDateCreation(LocalDateTime.now());
		entity.setId(1L);
		entity.setDescription("description4Test");
		entity.setPhoto(null);
		
		InteretsReadDTO dto = InteretsConverter.convertToDTO(entity);
		
		assertThat(dto.getDateCrea()).startsWith(LocalDate.now().toString());
		assertThat(dto).hasFieldOrPropertyWithValue("id", 1L).hasFieldOrPropertyWithValue("description", "description4Test").hasFieldOrPropertyWithValue("idPhoto", null);
	
	}
	
	@Test
	void testConvertNullDateCreaToDto_shouldReturnValidReadDto(){
		Interets entity = new Interets();
		Photo photo = new Photo();
		photo.setId(1L);
		
		entity.setDateCreation(null);
		entity.setId(1L);
		entity.setDescription("description4Test");
		entity.setPhoto(photo);
		
		InteretsReadDTO dto = InteretsConverter.convertToDTO(entity);
		
		assertThat(dto.getDateCrea()).isNull();
		assertThat(dto).hasFieldOrPropertyWithValue("id", 1L).hasFieldOrPropertyWithValue("description", "description4Test").hasFieldOrPropertyWithValue("idPhoto",1L);
	
	}
	
	@Test
	void testConvertNullEntityToDto_shouldReturnNull() {
		
		InteretsReadDTO dto = InteretsConverter.convertToDTO(null);
		
		assertThat(dto).isNull();
		
	}
	
	
	@Test
	void testConvertValidListEntityToListDto_shouldReturnValidListReadDto(){
		List<Interets> lstentity = new ArrayList<Interets>();
		Interets inter1 = new Interets();
		lstentity.add(inter1);
		lstentity.add(inter1);
		
		
		List<InteretsReadDTO> lstdto = InteretsConverter.convertToListDTO(lstentity);
		
		assertThat(lstdto).asList().hasSameSizeAs(lstentity);
		
		
	
	}
	
	@Test
	void testConvertNullListEntityToListDto_shouldReturnNullListReadDto(){
		List<Interets> lstentity = new ArrayList<Interets>();
		lstentity = null;
		
		List<InteretsReadDTO> lstdto = InteretsConverter.convertToListDTO(lstentity);
		
		assertThat(lstdto).isNull();
	
	}
	
	
	
}
