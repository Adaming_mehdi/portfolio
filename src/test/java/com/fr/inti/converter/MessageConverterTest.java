package com.fr.inti.converter;

import org.junit.jupiter.api.Test;



import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;


import com.fr.inti.dto.MessageCreateDTO;
import com.fr.inti.dto.MessageReadDTO;
import com.fr.inti.dto.MessageUpdateDTO;

import com.fr.inti.dto.RecruterReadDTO;
import com.fr.inti.dto.UserReadDTO;

import com.fr.inti.entity.Message;

import com.fr.inti.entity.Recruter;
import com.fr.inti.entity.User;

public class MessageConverterTest {
	// DTO TO ENTITY

	// Valid Attributes

		@Test
		void testConvertValidReadDtoToEntity_shouldReturnValidEntity() {
			MessageReadDTO dto = new MessageReadDTO();
			UserReadDTO dtoUser = new UserReadDTO();
			dtoUser.setId(1L);
			RecruterReadDTO dtoRecruter = new RecruterReadDTO();
			dtoRecruter.setId(1L);

			dto.setId((long) 1);
			dto.setContentMessage("content");
			dto.setUser(dtoUser);
			dto.setRecruter(dtoRecruter);
			dto.setDeleted(false);
			dto.setDateCreation(LocalDateTime.now().toString());

			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNotNull();
			assertThat(message.getId());
			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(message.getContentMessage()).isEqualTo(dto.getContentMessage());
			assertThat(message.getRecruter()).isEqualTo(dto.getIdRecruter());
			assertThat(message.getUser()).isEqualTo(dto.getIdUser());
			assertThat(message.isDeleted()).isEqualTo(dto.isDeleted());
			assertThat(message.getDateCreation().toString()).isEqualTo(dto.getDateCreation());

		}

		@Test
		void testConvertValidCreateDtoToEntity_shouldReturnValidEntity() {
			MessageCreateDTO dto = new MessageCreateDTO();

			dto.setContentMessage("content");
			dto.setIdUser((long) 1);
			dto.setIdRecruter((long) 1);

			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNotNull();

			assertThat(message.getId());
			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(message.getContentMessage()).isEqualTo(dto.getContentMessage());
			assertThat(message.getUser().getId()).isEqualTo(dto.getIdUser());
			assertThat(message.getRecruter().getId()).isEqualTo(dto.getIdRecruter());
		}
		
		@Test
		void testConvertValidUpdateDtoToEntity_shouldReturnValidEntity() {
			MessageUpdateDTO dto = new MessageUpdateDTO();

			dto.setId((long) 1);
			dto.setContentMessage("content");
			dto.setIdUser((long) 1);
			dto.setIdRecruter((long) 1);

			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNotNull();

			assertThat(message.getId());
			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(message.getContentMessage()).isEqualTo(dto.getContentMessage());
			assertThat(message.getUser().getId()).isEqualTo(dto.getIdUser());
			assertThat(message.getRecruter().getId()).isEqualTo(dto.getIdRecruter());
		}
		
	// Null objects

		@Test
		void testConvertNullReadDtoToEntity_shouldReturnNull() {
			MessageReadDTO dto = null;
			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNull();
		}
		
		@Test
		void testConvertNullListDtoToEntity_shouldReturnNull() {
			List<Message> message = MessageConverter.convertToEntityList(null);

			assertThat(message).isNull();
		}

		@Test
		void testConvertNullCreateDtoToEntity_shouldReturnNull() {
			MessageCreateDTO dto = null;
			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNull();
		}

		@Test
		void testConvertNullUpdateDtoToEntity_shouldReturnNull() {
			MessageUpdateDTO dto = null;
			Message message = MessageConverter.convertToEntity(dto);

			assertThat(message).isNull();
		}
	
	// ENTITY TO DTO --------------------------------------------------------------------------------------------

	// Valid Attributes

		@Test
		void testConvertValidReadEntityToDto_shouldReturnValidEntity() {
			Message message = new Message();
			User messageUser = new User();
			//messageUser.setVisible(true);
			messageUser.setDateCreation(LocalDateTime.now());
			Recruter messageRecruter = new Recruter();

			message.setId((long) 1);
			message.setContentMessage("content");
			message.setUser(messageUser);
			message.setRecruter(messageRecruter);
			message.setDeleted(false);
			message.setDateCreation(LocalDateTime.now());

			MessageReadDTO dto = MessageConverter.convertToReadDTO(message);

			assertThat(dto).isNotNull();

			assertThat(message.getId());
			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(message.getContentMessage()).isEqualTo(dto.getContentMessage());
			assertThat(message.getUser().getId()).isEqualTo(dto.getIdUser());
			assertThat(message.getRecruter().getId()).isEqualTo(dto.getIdRecruter());
		}
		
		@Test
		void testConvertValidCreateEntityToDto_shouldReturnValidEntity() {
			Message message = new Message();
			User messageUser= new User();
			Recruter messageRecruter = new Recruter();

			message.setContentMessage("content");
			message.setUser(messageUser);
			message.setRecruter(messageRecruter);

			MessageCreateDTO dto = MessageConverter.convertToCreateDTO(message);

			assertThat(dto).isNotNull();

			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(dto.getIdUser()).isEqualTo(message.getUser().getId());
			assertThat(dto.getIdRecruter()).isEqualTo(message.getRecruter().getId());
		}
		
		@Test
		void testConvertValidUpdateEntityToDto_shouldReturnValidEntity() {
			Message message = new Message();
			User messageUser = new User();
			Recruter messageRecruter = new Recruter();

			message.setId((long) 1);
			message.setContentMessage("content");
			message.setUser(messageUser);
			message.setRecruter(messageRecruter);

			MessageUpdateDTO dto = MessageConverter.convertToUpdateDTO(message);

			assertThat(dto).isNotNull();

			assertThat(dto).hasFieldOrPropertyWithValue("id", message.getId());
			assertThat(dto).hasFieldOrPropertyWithValue("contentMessage", message.getContentMessage());
			assertThat(dto.getIdUser()).isEqualTo(message.getUser().getId());
			assertThat(dto.getIdRecruter()).isEqualTo(message.getRecruter().getId());
		}

	// Null objects
		
		@Test
		void testConvertNullReadEntityToDto_shouldReturnNull() {
			MessageReadDTO dto = MessageConverter.convertToReadDTO(null);

			assertThat(dto).isNull();
		}

		@Test
		void testConvertNullListEntityToDto_shouldReturnNull() {
			List<MessageReadDTO> dto = MessageConverter.convertToDtoList(null);

			assertThat(dto).isNull();
		}

		@Test
		void testConvertNullCreateEntityToDto_shouldReturnNull() {
			MessageCreateDTO dto = MessageConverter.convertToCreateDTO(null);

			assertThat(dto).isNull();
		}

		@Test
		void testConvertNullUpdateEntityToDto_shouldReturnNull() {
			MessageUpdateDTO dto = MessageConverter.convertToUpdateDTO(null);

			assertThat(dto).isNull();
		}
	
}
