package com.fr.inti.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import org.junit.jupiter.api.Test;
import com.fr.inti.dto.PhotoCreateDTO;
import com.fr.inti.entity.Photo;




public class PhotoConverterTest {

	@Test
	public void testConvertValidCreatetoToEntity_shouldReturnPhotoEntity() {
		PhotoCreateDTO dto = new PhotoCreateDTO();

		dto.setUrlPhoto("urlPhoto");



		Photo entity = PhotoConverter.convertToEntity(dto);
		
		
		assertNotNull(entity);
		assertEquals(dto.getUrlPhoto(), entity.getUrlPhoto());
		assertFalse(entity.isDeleted());


	}


	
	

}