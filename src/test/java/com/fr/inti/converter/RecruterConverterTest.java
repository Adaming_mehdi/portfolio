package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.RecruterCreateDTO;
import com.fr.inti.dto.RecruterReadDTO;
import com.fr.inti.entity.Recruter;

public class RecruterConverterTest {

	@Test
	// Test de la méthode convertToEntity(RecruterCreateDTO) avec DTO not null
	void testValidConvertCreateDtoToEntity_shouldReturnValidEntity() {
		// Context
		RecruterCreateDTO dto = new RecruterCreateDTO();

		dto.setNom("Nomdetest");
		dto.setPrenom("Prenomdetest");
		dto.setLogin("Logindetest");
		dto.setPassword("Pswdetest");
		dto.setAvatarUrl("Avatardetest");

		// Test
		Recruter entity = RecruterConverter.convertCreateDTOToEntity(dto);

		// Assert
		assertNotNull(entity);
		assertEquals(dto.getNom(), entity.getNom());
		assertEquals(dto.getPrenom(), entity.getPrenom());
		assertEquals(dto.getLogin(), entity.getLogin());
		assertEquals(dto.getPassword(), entity.getPassword());
		assertEquals(dto.getAvatarUrl(), entity.getAvatarUrl());
	}

	@Test
	// Test de la méthode convertToEntity((RecruterCreateDTO) avec DTO null
	void testValidConvertCreateDtoToEntityWithNullDto_shouldReturnNull() {
		// Context
		// Test
		Recruter entity = RecruterConverter.convertCreateDTOToEntity(null);

		// Assert
		assertThat(entity).isNull();
	}

	@Test
	// Test de la méthode convertToEntity((RecruterReadDTO) avec DTO not null
	void testValidConvertReadDtoToEntity_shouldReturnValidEntity() {
		// Context
		RecruterReadDTO dto = new RecruterReadDTO();

		dto.setNom("Nomdetest");
		dto.setPrenom("Prenomdetest");
		dto.setLogin("Logindetest");
		dto.setPassword("Pswdetest");
		dto.setAvatarUrl("Avatardetest");

		// Test
		Recruter entity = RecruterConverter.convertReadDTOToEntity(dto);

		// Assert
		assertNotNull(entity);
		assertEquals(dto.getNom(), entity.getNom());
		assertEquals(dto.getPrenom(), entity.getPrenom());
		assertEquals(dto.getLogin(), entity.getLogin());
		assertEquals(dto.getPassword(), entity.getPassword());
		assertEquals(dto.getAvatarUrl(), entity.getAvatarUrl());
	}

	@Test
	// Test de la méthode convertToEntity((RecruterReadDTO) avec DTO null
	void testValidConvertReadDtoToEntityWithNullDto_shouldReturnNull() {
		// Context
		// Test
		Recruter entity = RecruterConverter.convertReadDTOToEntity(null);

		// Assert
		assertThat(entity).isNull();
	}

	@Test
	// Test de la méthode convertToDTO(Recruter) avec entity not null
	void testValidConvertEntityToReadDto_shouldReturnValidEntity() {
		// Context
		Recruter entity = new Recruter();

		entity.setId(0L);
		entity.setNom("Nomdetest");
		entity.setPrenom("Prenomdetest");
		entity.setLogin("Logindetest");
		entity.setPassword("Pswdetest");
		entity.setAvatarUrl("Avatardetest");

		// Test
		RecruterReadDTO dto = RecruterConverter.convertToDTO(entity);

		// Assert
		assertNotNull(dto);
		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getNom(), entity.getNom());
		assertEquals(dto.getPrenom(), entity.getPrenom());
		assertEquals(dto.getLogin(), entity.getLogin());
		assertEquals(dto.getPassword(), entity.getPassword());
		assertEquals(dto.getAvatarUrl(), entity.getAvatarUrl());
	}

	@Test
	// Test de la méthode convertToDTO(Recruter) avec entity null
	void testValidConvertEntityToReadDtoWithNullEntity_shouldReturnNull() {
		// Context
		// Test
		RecruterReadDTO dto = RecruterConverter.convertToDTO(null);

		// Assert
		assertThat(dto).isNull();
	}

	@Test
	// Test de la méthode convertToDtoList(List<Recruter>) avec list not null
	void testValidConvertEntityListToDtoList_shouldReturnValidList() {
		// Context
		Recruter entity = new Recruter();
		entity.setId(0L);
		entity.setNom("Nomdetest");
		entity.setPrenom("Prenomdetest");
		entity.setLogin("Logindetest");
		entity.setPassword("Pswdetest");
		entity.setAvatarUrl("Avatardetest");
		List<Recruter> recruters = new ArrayList<Recruter>();
		recruters.add(entity);

		// Test
		List<RecruterReadDTO> dtoList = RecruterConverter.convertToDtoList(recruters);

		// Assert
		assertNotNull(dtoList.get(0));
		assertEquals(dtoList.get(0).getId(), entity.getId());
		assertEquals(dtoList.get(0).getNom(), entity.getNom());
		assertEquals(dtoList.get(0).getPrenom(), entity.getPrenom());
		assertEquals(dtoList.get(0).getLogin(), entity.getLogin());
		assertEquals(dtoList.get(0).getPassword(), entity.getPassword());
		assertEquals(dtoList.get(0).getAvatarUrl(), entity.getAvatarUrl());
	}

	@Test
	// Test de la méthode convertToDtoList(List<Recruter>) avec list null
	void testValidConvertEntityListToDtoListWithNullList_shouldReturnEmptyList() {
		// Context
		List<Recruter> recruters = new ArrayList<Recruter>();

		// Test
		List<RecruterReadDTO> dtoList = RecruterConverter.convertToDtoList(recruters);

		// Assert
		assertThat(dtoList).isEmpty();
	}

	@Test
	// Test de la méthode convertToEntityList(List<RecruterReadDTO>) avec list not
	// null
	void testValidConvertDtoListToEntityList_shouldReturnValidList() {
		// Context
		RecruterReadDTO dto = new RecruterReadDTO();
		dto.setId(0L);
		dto.setNom("Nomdetest");
		dto.setPrenom("Prenomdetest");
		dto.setLogin("Logindetest");
		dto.setPassword("Pswdetest");
		dto.setAvatarUrl("Avatardetest");
		List<RecruterReadDTO> dtoList = new ArrayList<RecruterReadDTO>();
		dtoList.add(dto);

		// Test
		List<Recruter> recruters = RecruterConverter.convertToEntityList(dtoList);

		// Assert
		assertNotNull(recruters.get(0));
		// assertEquals(recruters.get(0).getId(), dto.getId());
		assertEquals(recruters.get(0).getNom(), dto.getNom());
		assertEquals(recruters.get(0).getPrenom(), dto.getPrenom());
		assertEquals(recruters.get(0).getLogin(), dto.getLogin());
		assertEquals(recruters.get(0).getPassword(), dto.getPassword());
		assertEquals(recruters.get(0).getAvatarUrl(), dto.getAvatarUrl());
	}

	@Test
	// Test de la méthode convertToDtoList(List<Recruter>) avec list null
	void testValidConvertDtoListToEntityListWithNullList_shouldReturnEmptyList() {
		// Context
		List<RecruterReadDTO> dtoList = new ArrayList<RecruterReadDTO>();

		// Test
		List<Recruter> recruters = RecruterConverter.convertToEntityList(dtoList);

		// Assert
		assertThat(recruters).isEmpty();
	}

}
