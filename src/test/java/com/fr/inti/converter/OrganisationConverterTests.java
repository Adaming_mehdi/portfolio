package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.OrganisationDTO;
import com.fr.inti.entity.Organisation;

class OrganisationConverterTests {

	@Test
	void testConvertOrganisationDtoToEntity_shouldReturnEntity() {

		OrganisationDTO dto = new OrganisationDTO();

		dto.setNom("nomTest");
		dto.setDescription("descTest");
		dto.setLienWeb("lienWebTest");

		Organisation entity = OrganisationConverter.convertToEntity(dto);

		assertNotNull(entity);
		assertEquals(entity.getNom(), dto.getNom());
		assertEquals(entity.getDescription(), dto.getDescription());
		assertEquals(entity.getLienWeb(), dto.getLienWeb());
		assertFalse(entity.isDeleted());

	}

	@Test
	void testConvertDtoNullToEntity_shouldReturnNull() {

		Organisation organisation = OrganisationConverter.convertToEntity(null);
		assertNull(organisation);

	}

	@Test
	void testConvertOrganisationToDto_shouldReturnDto() {

		Organisation entity = new Organisation();

		Long id = Long.valueOf(1);
		entity.setId(id);
		entity.setNom("nomTest");
		entity.setDescription("descTest");
		entity.setLienWeb("lienWebTest");

		OrganisationDTO dto = OrganisationConverter.convertToDto(entity);

		assertNotNull(dto);
		assertNotNull(dto.getId());
		assertEquals(dto.getId(), id);
		assertEquals(dto.getNom(), entity.getNom());
		assertEquals(dto.getDescription(), entity.getDescription());
		assertEquals(dto.getLienWeb(), entity.getLienWeb());

	}

	@Test
	void testConvertOrganisationNullToDto_shouldReturnNull() {

		OrganisationDTO dto = OrganisationConverter.convertToDto(null);
		assertNull(dto);

	}

	@Test
	void testConvertOrganisationListToDtoList_shouldReturnDtoList() {

		List<Organisation> entityList = new ArrayList<>();

		Organisation entity = new Organisation();
		Long id = Long.valueOf(1);
		entity.setId(id);
		entity.setNom("nomTest");
		entity.setDescription("descTest");
		entity.setLienWeb("lienWebTest");
		entityList.add(entity);

		Organisation entity2 = new Organisation();
		Long id2 = Long.valueOf(2);
		entity2.setId(id2);
		entity2.setNom("nomTest2");
		entity2.setDescription("descTest2");
		entity2.setLienWeb("lienWebTest2");
		entityList.add(entity2);

		List<OrganisationDTO> dtoList = OrganisationConverter.convertToDtoList(entityList);

		assertNotNull(dtoList.get(0));
		assertNotNull(dtoList.get(0).getId());
		assertEquals(dtoList.get(0).getId(), id);
		assertEquals(dtoList.get(0).getNom(), entity.getNom());
		assertEquals(dtoList.get(0).getDescription(), entity.getDescription());
		assertEquals(dtoList.get(0).getLienWeb(), entity.getLienWeb());

		assertNotNull(dtoList.get(1));
		assertNotNull(dtoList.get(1).getId());
		assertEquals(dtoList.get(1).getId(), id2);
		assertEquals(dtoList.get(1).getNom(), entity2.getNom());
		assertEquals(dtoList.get(1).getDescription(), entity2.getDescription());
		assertEquals(dtoList.get(1).getLienWeb(), entity2.getLienWeb());

	}

	@Test
	void testConvertOrganisationEmptyListToDtoList_shouldReturnEmptyList() {

		List<Organisation> organisationList = new ArrayList<>();
		List<OrganisationDTO> dtoList = OrganisationConverter.convertToDtoList(organisationList);
		assertThat(dtoList).isEmpty();

	}

	@Test
	void testConvertNullToDtoList_shouldReturnEmptyList() {

		List<OrganisationDTO> dtoList = OrganisationConverter.convertToDtoList(null);
		assertThat(dtoList).isEmpty();

	}

}
