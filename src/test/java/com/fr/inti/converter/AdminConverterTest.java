package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.AdminCreateDTO;
import com.fr.inti.dto.AdminReadDTO;
import com.fr.inti.dto.AdminUpdateDTO;
import com.fr.inti.entity.Admin;

class AdminConverterTest {
	
	@Test
	void testValidCreateEntityConversion_shouldReturnAdminEntity() {
		//Context
		AdminCreateDTO dto = new AdminCreateDTO();
		dto.setAvatarUrl("urlTest");
		dto.setLogin("loginTest");
		dto.setNom("nomTest");
		dto.setPassword("passwordTest");
		dto.setPrenom("prenomTest");
		
		//Test
		Admin admin = AdminConverter.convertToCreateEntity(dto);
		
		//Assert
		assertThat(admin).isNotNull();
		assertThat(admin.getAvatarUrl()).isEqualTo(dto.getAvatarUrl());
		assertThat(admin.getLogin()).isEqualTo(dto.getLogin());
		assertThat(admin.getNom()).isEqualTo(dto.getNom());
		assertThat(admin.getPrenom()).isEqualTo(dto.getPrenom());
		assertThat(admin.getPassword()).isEqualTo(dto.getPassword());

	}
	
	@Test
	void testValidCreateEntityConversionWithNullDto_shouldReturnNull() {
		//Test
		Admin admin = AdminConverter.convertToCreateEntity(null);	
				
		//Assert
		assertThat(admin).isNull();

	}
	
	@Test
	void testValidUpdateEntityConversion_shouldReturnAdminEntity() {
		//Context
		AdminUpdateDTO dto = new AdminUpdateDTO();
		dto.setAvatarUrl("urlTest");
		dto.setLogin("loginTest");
		dto.setNom("nomTest");
		dto.setPassword("passwordTest");
		dto.setPrenom("prenomTest");
		dto.setDeleted(false);
		dto.setId(1L);
		
		//Test
		Admin admin = AdminConverter.convertToUpdateEntity(dto);
		
		//Assert
		assertThat(admin).isNotNull();
		assertThat(admin.getAvatarUrl()).isEqualTo(dto.getAvatarUrl());
		assertThat(admin.getLogin()).isEqualTo(dto.getLogin());
		assertThat(admin.getNom()).isEqualTo(dto.getNom());
		assertThat(admin.getPrenom()).isEqualTo(dto.getPrenom());
		assertThat(admin.getPassword()).isEqualTo(dto.getPassword());
		assertThat(admin.getId()).isEqualTo(dto.getId());
		assertThat(admin.isDeleted()).isEqualTo(dto.isDeleted());

	}
	
	@Test
	void TestValidUpdateEntityConversionWithNullDto_shouldReturnNull() {
		//Test
		Admin admin = AdminConverter.convertToUpdateEntity(null);	
				
		//Assert
		assertThat(admin).isNull();

	}
	
	@Test
	void testValidReadDtoConversion_shouldReturnAdminReadDto() {
		//Context
		Admin admin = new Admin();
		admin.setAvatarUrl("urlTest");
		admin.setLogin("loginTest");
		admin.setNom("nomTest");
		admin.setPassword("passwordTest");
		admin.setPrenom("prenomTest");
		admin.setDeleted(false);
		admin.setDateCreation(LocalDateTime.parse("2020-11-19T00:00:00"));
		admin.setId(1L);
		
		//Test
		AdminReadDTO dto = AdminConverter.convertToReadDTO(admin);
		
		//Assert
		assertThat(dto).isNotNull();
		assertThat(dto.getAvatarUrl()).isEqualTo(admin.getAvatarUrl());
		assertThat(dto.getLogin()).isEqualTo(admin.getLogin());
		assertThat(dto.getNom()).isEqualTo(admin.getNom());
		assertThat(dto.getPrenom()).isEqualTo(admin.getPrenom());
		assertThat(dto.getDateCreation()).isEqualTo(admin.getDateCreation().toString());
		assertThat(dto.getId()).isEqualTo(admin.getId());
		assertThat(dto.isDeleted()).isEqualTo(admin.isDeleted());

	}
	
	@Test
	void testValidReadDtoConversionWithNullAdmin_shouldReturnNull() {
		//Test
		AdminReadDTO dto = AdminConverter.convertToReadDTO(null);	
				
		//Assert
		assertThat(dto).isNull();

	}
	
	@Test
	void testValidReadDtoListConversion_shouldReturnListOfAdminReadDto() {
		//Context
		Admin admin = new Admin();
		admin.setAvatarUrl("urlTest");
		admin.setLogin("loginTest");
		admin.setNom("nomTest");
		admin.setPassword("passwordTest");
		admin.setPrenom("prenomTest");
		admin.setDeleted(false);
		admin.setDateCreation(LocalDateTime.parse("2020-11-19T00:00:00"));
		admin.setId(1L);
		List<Admin> admins = new ArrayList<Admin>();
		admins.add(admin);
		
		//Test
		List<AdminReadDTO> dtoList = AdminConverter.convertToReadDtoList(admins);	
		
		//Assert
		assertThat(dtoList).asList().size().isEqualTo(1);
		
		assertThat(dtoList.get(0).getAvatarUrl()).isEqualTo(admin.getAvatarUrl());
		assertThat(dtoList.get(0).getLogin()).isEqualTo(admin.getLogin());
		assertThat(dtoList.get(0).getNom()).isEqualTo(admin.getNom());
		assertThat(dtoList.get(0).getPrenom()).isEqualTo(admin.getPrenom());
		assertThat(dtoList.get(0).getDateCreation()).isEqualTo(admin.getDateCreation().toString());
		assertThat(dtoList.get(0).getId()).isEqualTo(admin.getId());
		assertThat(dtoList.get(0).isDeleted()).isEqualTo(admin.isDeleted());

	}
	
	@Test
	void testValidReadDtoListConversionWithNullAdminList_shouldReturnNull() {
		//Test
		List<AdminReadDTO> dtoList = AdminConverter.convertToReadDtoList(null);	
				
		//Assert
		assertThat(dtoList).isNull();

	}
	
	@Test
	void testValidReadDtoListConversionWithEmptyAdminList_shouldReturnEmptyListOfAdmin() {
		//Context
		List<Admin> admins = new ArrayList<Admin>();
		
		//Test
		List<AdminReadDTO> dtoList = AdminConverter.convertToReadDtoList(admins);	
				
		//Assert
		assertThat(dtoList).asList().isEmpty();

	}
	

}
