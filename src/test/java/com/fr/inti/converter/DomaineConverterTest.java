package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;
import com.fr.inti.dto.DomaineDTO;
import com.fr.inti.entity.Domaine;

class DomaineConverterTest {

	@Test // DtoToEntity
	void testConvertValidDtoToEntity_shouldReturnValidEntity() {
		DomaineDTO dto = new DomaineDTO();
		dto.setLibelle("LibelleTest");
		Domaine entity = DomaineConverter.convertToEntity(dto);
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("libelle", dto.getLibelle());
		assertFalse(entity.isDeleted());
	}

	@Test // DtoToEntity Null
	void testConvertNullDtoToEntity_shouldRetunrNull() {
		Domaine entity = DomaineConverter.convertToEntity(null);
		assertNull(entity);
	}

	@Test // EntityToDto
	void testConvertValidEntityToDto_shouldReturnValidDto() {
		Domaine entity = new Domaine();
		entity.setLibelle("LibelleTest");
		DomaineDTO dto = DomaineConverter.convertToDTO(entity);
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("libelle", entity.getLibelle());
	}

	@Test // DtoToEntity Null
	void testConvertNullEntityToDto_shouldRetunrNull() {
		DomaineDTO dto = DomaineConverter.convertToDTO(null);
		assertNull(dto);
	}

	@Test // ConvertDtoList
	void testConvertDomaineListToDtoList_shouldReturnDtoList() {

		List<Domaine> entityList = new ArrayList<>();

		Domaine entity = new Domaine();
		Long id = Long.valueOf(1);
		entity.setId(id);
		entity.setLibelle("libelleTest1");
		entityList.add(entity);

		Domaine entity2 = new Domaine();
		Long id2 = Long.valueOf(2);
		entity2.setId(id2);
		entity2.setLibelle("libelleTest2");
		entityList.add(entity2);

		List<DomaineDTO> dtoList = DomaineConverter.convertToDtoList(entityList);

		assertNotNull(dtoList.get(0));
		assertEquals(dtoList.get(0).getLibelle(), entity.getLibelle());

		assertNotNull(dtoList.get(1));
		assertEquals(dtoList.get(1).getLibelle(), entity2.getLibelle());

	}

	@Test // DtoListNull
	void TestValidReadDtoListConversionWithNullAdminList_shouldReturnNull() {
		List<DomaineDTO> dtoList = DomaineConverter.convertToDtoList(null);
		assertThat(dtoList).isEmpty();

	}

	@Test // DtoListEmpty
	void testConvertOrganisationEmptyListToDtoList_shouldReturnEmptyList() {

		List<Domaine> domaineList = new ArrayList<>();
		List<DomaineDTO> dtoList = DomaineConverter.convertToDtoList(domaineList);
		assertThat(dtoList).isEmpty();

	}

	@Test // EntityList
	void testValidConvertDtoListToEntityList_shouldReturnValidList() {
		DomaineDTO dto = new DomaineDTO();
		dto.setLibelle("libelleTest");
		List<DomaineDTO> dtoList = new ArrayList<DomaineDTO>();
		dtoList.add(dto);
		List<Domaine> domaines = DomaineConverter.convertToEntityList(dtoList);
		assertNotNull(domaines.get(0));
		assertEquals(domaines.get(0).getLibelle(), dto.getLibelle());

	}

	@Test // EntityList null
	void testValidConvertDtoListToEntityListWithNullList_shouldNullList() {
		List<Domaine> domaines = DomaineConverter.convertToEntityList(null);
		assertThat(domaines).isEmpty();
	}

}
