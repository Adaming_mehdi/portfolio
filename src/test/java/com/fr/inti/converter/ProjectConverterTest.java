package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.ProjectCreateDTO;
import com.fr.inti.dto.ProjectReadDTO;
import com.fr.inti.entity.Project;

 class ProjectConverterTest {

	@Test
	 void testValidCreateEntityConversion_shouldReturnProjectEntity() throws ParseException {

		// Context
		ProjectCreateDTO dto = new ProjectCreateDTO();
		dto.setNom("nom");
		dto.setDescription("description");
		dto.setDateDebut("2020-01-01");
		dto.setDateFin("2020-01-31");

		// Test
		Project project = ProjectConverter.convertToCreateEntity(dto);

		// Assert
		assertThat(project).isNotNull();
		assertThat(project.getNom()).isEqualTo(dto.getNom());
		assertThat(project.getDescription()).isEqualTo(dto.getDescription());
		assertThat(project.getDateDebut().toString()).hasToString(dto.getDateDebut());
		assertThat(project.getDateFin().toString()).hasToString(dto.getDateFin());

	}

	@Test
	 void testValidCreateEntityConversionWithNullDto_shouldReturnNull() throws ParseException {

		// Test
		Project project = ProjectConverter.convertToCreateEntity(null);

		// Assert
		assertThat(project).isNull();

	}

	@Test
	 void testValidReadEntityConversion_shouldReturnProjectEntity() throws ParseException {

		// Context
		ProjectReadDTO dto = new ProjectReadDTO();
		dto.setNom("nom");
		dto.setDescription("description");
		dto.setDateDebut("2020-01-01");
		dto.setDateFin("2020-01-31");
		dto.setDeleted(false);
		dto.setId(1L);

		// Test
		Project project = ProjectConverter.convertToReadEntity(dto);

		// Assert
		assertThat(project).isNotNull();
		assertThat(project.getNom()).isEqualTo(dto.getNom());
		assertThat(project.getDescription()).isEqualTo(dto.getDescription());
		assertThat(project.getDateDebut().toString()).hasToString(dto.getDateDebut());
		assertThat(project.getDateFin().toString()).hasToString(dto.getDateFin());
		assertThat(project.getId()).isEqualTo(dto.getId());
		assertThat(project.isDeleted()).isEqualTo(dto.isDeleted());

	}

	@Test
	 void testValidReadDtoConversionWithNullProject_shouldReturnNull() {

		// Test
		ProjectReadDTO dto = ProjectConverter.convertToReadDTO(null);

		// Assert
		assertThat(dto).isNull();

	}

	@Test
	 void testValidReadEntityConversion_shouldReturnProjectReadDTO() throws ParseException {

		// Context
		Project project = new Project();
		project.setNom("nom");
		project.setDescription("description");
		project.setDateDebut(LocalDate.parse("2020-01-01"));
		project.setDateFin(LocalDate.parse("2020-01-31"));
		project.setDeleted(false);
		project.setId(1L);

		// Test

		ProjectReadDTO dto = ProjectConverter.convertToReadDTO(project);

		// Assert
		assertThat(dto).isNotNull();
		assertThat(dto.getNom()).isEqualTo(project.getNom());
		assertThat(dto.getDescription()).isEqualTo(project.getDescription());
		assertThat(dto.getDateDebut()).isEqualTo(project.getDateDebut().toString());
		assertThat(dto.getDateFin()).isEqualTo(project.getDateFin().toString());
		assertThat(dto.getId()).isEqualTo(project.getId());
		assertThat(dto.isDeleted()).isEqualTo(project.isDeleted());

	}

}
