package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.inti.dto.EtudeCreateDTO;
import com.fr.inti.dto.EtudeReadDTO;
import com.fr.inti.dto.EtudeUpdateDTO;
import com.fr.inti.entity.Domaine;
import com.fr.inti.entity.Etude;
import com.fr.inti.entity.Organisation;
import com.fr.inti.entity.Photo;

public class EtudeConverterTests {
	
	@Test
	public void testConvertEtudeCreateDtoToEntity_shouldReturnEntity() {
		
		EtudeCreateDTO dto = new EtudeCreateDTO();
		
		dto.setNom("nomTest");
		dto.setDescription("descTest");
		dto.setDateDebut("2020-01-01");
		dto.setDateFin("2020-01-01");
		dto.setDomaineId(Long.valueOf(1));
		dto.setOrganisationId(Long.valueOf(1));
		dto.setPortfolioId(Long.valueOf(1));
		dto.setPhotoId(Long.valueOf(1));
		
		Etude entity = EtudeConverter.convertToEntity(dto);
		
		assertNotNull(entity);
		assertEquals(entity.getNom(), dto.getNom());
		assertEquals(entity.getDescription(), dto.getDescription());
		assertNotNull(entity.getDateDebut());
		assertThat(entity.getDateDebut()).isInstanceOf(LocalDate.class);
		assertNotNull(entity.getDateFin());
		assertThat(entity.getDateFin()).isInstanceOf(LocalDate.class);
		assertFalse(entity.isDeleted());
		assertNotNull(entity.getOrganisation());
		assertNotNull(entity.getDomaine());
		assertNotNull(entity.getPortfolio());
		assertNotNull(entity.getPhoto());
		
	}
	
	@Test
	public void testConvertDtoNullToEntity_shouldReturnNull() {
		
		Etude etude = EtudeConverter.convertToEntity(null);
		assertNull(etude);
		
	}
	
	@Test
	public void testConvertEtudeUpdateDtoToEntity_shouldReturnEntity() {
		
		EtudeUpdateDTO dto = new EtudeUpdateDTO();
		
		dto.setNom("nomTest");
		dto.setDescription("descTest");
		dto.setDateDebut("2020-01-01");
		dto.setDateFin("2020-01-01");
		dto.setDomaineId(Long.valueOf(1));
		dto.setOrganisationId(Long.valueOf(1));
		dto.setPortfolioId(Long.valueOf(1));
		dto.setPhotoId(Long.valueOf(1));
		
		Etude entity0 = new Etude();
		
		entity0.setId(Long.valueOf(1));
		entity0.setNom("nom0");
		entity0.setDescription("desc0");
		
		Etude entity = EtudeConverter.convertToEntity(dto, entity0);
		
		assertNotNull(entity);
		assertEquals(entity.getId(), 1);
		assertEquals(entity.getNom(), dto.getNom());
		assertEquals(entity.getDescription(), dto.getDescription());
		assertNotNull(entity.getDateDebut());
		assertThat(entity.getDateDebut()).isInstanceOf(LocalDate.class);
		assertNotNull(entity.getDateFin());
		assertThat(entity.getDateFin()).isInstanceOf(LocalDate.class);
		assertFalse(entity.isDeleted());
		assertNotNull(entity.getOrganisation());
		assertNotNull(entity.getDomaine());
		assertNotNull(entity.getPortfolio());
		assertNotNull(entity.getPhoto());
		
	}
	
	@Test
	public void testConvertEtudeToDto_shouldReturnDto() {
		
		Etude entity = new Etude();
		
		Long id = Long.valueOf(1);
		entity.setId(id);
		entity.setNom("nomTest");
		entity.setDescription("descTest");
		entity.setDateDebut(LocalDate.now());
		entity.setDateFin(LocalDate.now());
		Domaine domaine = new Domaine();
		domaine.setLibelle("libDomaine");
		entity.setDomaine(domaine);
		Organisation organisation = new Organisation();
		organisation.setNom("nomOrg");
		entity.setOrganisation(organisation);
		Photo photo = new Photo();
		photo.setUrlPhoto("urlPhoto");
		entity.setPhoto(photo);
		
		EtudeReadDTO dto = EtudeConverter.convertToDto(entity);
		
		assertNotNull(dto);
		assertNotNull(dto.getId());
		assertEquals(dto.getId(), 1);
		assertEquals(dto.getNom(), entity.getNom());
		assertEquals(dto.getDescription(), entity.getDescription());
		assertEquals(dto.getDateDebut(), LocalDate.now().toString());
		assertEquals(dto.getDateFin(), LocalDate.now().toString());
		assertEquals(dto.getNomDomaine(), domaine.getLibelle());
		assertEquals(dto.getNomOrganisation(), organisation.getNom());
		assertEquals(dto.getPhotoUrl(), photo.getUrlPhoto());
		
	}
	
	@Test
	public void testConvertEtudeNullToDto_shouldReturnNull() {
		
		EtudeReadDTO dto = EtudeConverter.convertToDto(null);
		assertNull(dto);
		
	}
	
	@Test
	public void testConvertEtudeListToDtoList_shouldReturnDtoList() {
		
		List<Etude> entityList = new ArrayList<>();
		
		Etude entity = new Etude();
		Long id = Long.valueOf(1);
		entity.setId(id);
		entity.setNom("nomTest");
		entity.setDescription("descTest");
		entity.setDateDebut(LocalDate.now());
		entity.setDateFin(LocalDate.now());
		entityList.add(entity);
		
		Etude entity2 = new Etude();
		Long id2 = Long.valueOf(2);
		entity2.setId(id2);
		entity2.setNom("nomTest2");
		entity2.setDescription("descTest2");
		entity2.setDateDebut(LocalDate.now());
		entity2.setDateFin(LocalDate.now());
		entityList.add(entity2);
		
		List<EtudeReadDTO> dtoList = EtudeConverter.convertToDtoList(entityList);
		
		assertNotNull(dtoList.get(0));
		assertNotNull(dtoList.get(0).getId());
		assertEquals(dtoList.get(0).getId(), 1);
		assertEquals(dtoList.get(0).getNom(), entity.getNom());
		assertEquals(dtoList.get(0).getDescription(), entity.getDescription());
		assertEquals(dtoList.get(0).getDateDebut(), LocalDate.now().toString());
		assertEquals(dtoList.get(0).getDateFin(), LocalDate.now().toString());
		
		assertNotNull(dtoList.get(1));
		assertNotNull(dtoList.get(1).getId());
		assertEquals(dtoList.get(1).getId(), 2);
		assertEquals(dtoList.get(1).getNom(), entity2.getNom());
		assertEquals(dtoList.get(1).getDescription(), entity2.getDescription());
		assertEquals(dtoList.get(1).getDateDebut(), LocalDate.now().toString());
		assertEquals(dtoList.get(1).getDateFin(), LocalDate.now().toString());
		
	}
	
	@Test
	public void testConvertEtudeEmptyListToDtoList_shouldReturnEmptyList() {
		
		List<Etude> etudeList = new ArrayList<>();
		List<EtudeReadDTO> dtoList = EtudeConverter.convertToDtoList(etudeList);
		assertThat(dtoList).isEmpty();
		
	}
	
	@Test
	public void testConvertNullToDtoList_shouldReturnEmptyList() {
		
		List<EtudeReadDTO> dtoList = EtudeConverter.convertToDtoList(null);
		assertThat(dtoList).isEmpty();
		
	}

}
