package com.fr.inti.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import com.fr.inti.dto.FavorisCreateDTO;
import com.fr.inti.dto.FavorisReadDTO;
import com.fr.inti.entity.Favoris;

//@SpringBootTest
class FavorisConverterTests {
	//@Autowired
	private FavorisConverter faconv;
	
	@SuppressWarnings("static-access")
	@Test
	void testConvtoDTO_shouldReturnCreateDTO()  {
		Favoris f = new Favoris();
		f.setId((long) 1);
		f.setLibelle("test");		
		f.setDeleted(false);
		f.setDateCreation(LocalDateTime.parse("2020-11-20T00:00:00"));
		FavorisCreateDTO fdto = new FavorisCreateDTO();
		
		fdto = faconv.convertToDTO(f);
		
		assertEquals(f.getId(),fdto.getId());
		assertEquals(f.getLibelle(),fdto.getLibelle());
		assertEquals(f.getDomaines(),fdto.getDomaines());
		assertEquals(f.getRecruteurs(),fdto.getRecruteurs());
		assertEquals(f.getDateCreation(),fdto.getDateCreation());
		assertEquals(f.isDeleted(),fdto.isDeleted());
		
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoEntity_shouldReturnEntity() {
		FavorisCreateDTO fdto= new FavorisCreateDTO();
		fdto.setId((long) 1);
		fdto.setLibelle("test");	
		fdto.setDeleted(false);		
		Favoris f = new Favoris();
		
		f = faconv.convertToEntity(fdto);
		
		assertEquals(fdto.getId(),f.getId());
		assertEquals(fdto.getLibelle(),f.getLibelle());
		assertEquals(fdto.getDomaines(),f.getDomaines());
		assertEquals(fdto.getRecruteurs(),f.getRecruteurs());
		assertEquals(fdto.getDateCreation(),f.getDateCreation());
		assertEquals(fdto.isDeleted(),fdto.isDeleted());
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoEntityReadOnly_shouldReturnEntity() {
		FavorisReadDTO fdto= new FavorisReadDTO();
		
		fdto.setLibelle("test");	
			
		Favoris f = new Favoris();
		
		f = faconv.convertToEntity(fdto);
		
		
		assertEquals(fdto.getLibelle(),f.getLibelle());
		assertEquals(fdto.getDomaines(),f.getDomaines());
		assertEquals(fdto.getRecruteurs(),f.getRecruteurs());
		
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoDTOReadOnly_shouldReturnReadOnlyDTO() {
		Favoris f= new Favoris();
		
		f.setLibelle("test");	
			
		FavorisReadDTO fdto = new FavorisReadDTO();
		
		fdto = faconv.convertToDTOReadOnly(f);
		
		
		assertEquals(fdto.getLibelle(),f.getLibelle());
		assertEquals(fdto.getDomaines(),f.getDomaines());
		assertEquals(fdto.getRecruteurs(),f.getRecruteurs());
		
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToCreateDTOlist_shouldReturnCreateDTOList() {
		List<Favoris> favoris = new ArrayList<Favoris>();
		Favoris favoris1 = new Favoris ();
		favoris1.setId((long) 1);
		favoris1.setLibelle("test");
		favoris1.setDeleted(false);
		favoris1.setDateCreation(LocalDateTime.parse("2020-11-20T00:00:00"));
		
		favoris.add(favoris1);
		
		
		List<FavorisCreateDTO> favDTOlist = faconv.convertToDtoList(favoris);
		
		assertThat(favDTOlist).asList().size().isEqualTo(1);
}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoEntitylist_shouldReturnEntityList() {
		List<FavorisCreateDTO> favorisdto = new ArrayList<FavorisCreateDTO>();
		FavorisCreateDTO favorisdto1 = new FavorisCreateDTO ();
		favorisdto1.setId((long) 1);
		favorisdto1.setLibelle("test");
		favorisdto1.setDeleted(false);
		favorisdto1.setDateCreation(LocalDateTime.parse("2020-11-20T00:00:00"));
		favorisdto.add(favorisdto1);
		
		List<Favoris> favDTOlist = faconv.convertToEntityList(favorisdto);
		
		assertThat(favDTOlist).asList().size().isEqualTo(1);
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoReadDTOlist_shouldReturnReadDTOList() {
		List<Favoris> favoris = new ArrayList<Favoris>();
		Favoris favoris1 = new Favoris ();
		favoris1.setId((long) 1);
		favoris1.setLibelle("test");
		favoris1.setDeleted(false);
		favoris1.setDateCreation(LocalDateTime.parse("2020-11-20T00:00:00"));
		favoris.add(favoris1);
		
		List<FavorisReadDTO> favDTOlist = faconv.convertToDtoListReadOnly(favoris);
		
		assertThat(favDTOlist).asList().size().isEqualTo(1);
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvtoEntitylistReadOnly_shouldReturnEntityList() {
		List<FavorisReadDTO> favorisdto = new ArrayList<FavorisReadDTO>();
		FavorisReadDTO favorisdto1 = new FavorisReadDTO ();
		
		favorisdto1.setLibelle("test");
		
		favorisdto.add(favorisdto1);
		
		List<Favoris> favDTOlist = faconv.convertToEntityListReadOnly(favorisdto);
		
		assertThat(favDTOlist).asList().size().isEqualTo(1);
		
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToDTOListEmpty_shouldReturnEmptyList() {
		List<Favoris> favoris = new ArrayList<Favoris>();
		
		
		List<FavorisCreateDTO> favDTOlist = faconv.convertToDtoList(favoris);
		
		assertThat(favDTOlist).asList().isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToEntityListEmpty_shouldReturnEmptyList() {
		List<FavorisCreateDTO> favorisdto = new ArrayList<FavorisCreateDTO>();
		
		
		List<Favoris> favlist = faconv.convertToEntityList(favorisdto);
		
		assertThat(favlist).asList().isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToReadDTOListEmpty_shouldReturnEmptyList() {
		List<Favoris> favoris = new ArrayList<Favoris>();
		
		
		List<FavorisReadDTO> favDTOlist = faconv.convertToDtoListReadOnly(favoris);
		
		assertThat(favDTOlist).asList().isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToReadEntityListEmpty_shouldReturnEmptyList() {
		List<FavorisReadDTO> favorisdto = new ArrayList<FavorisReadDTO>();
		
		
		List<Favoris> favlist = faconv.convertToEntityListReadOnly(favorisdto);
		
		assertThat(favlist).asList().isEmpty();
	
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToDTOListNull_shouldReturnEmpty() {
	
		List<FavorisCreateDTO> favDTOlist = faconv.convertToDtoList(null);
		
		assertThat(favDTOlist).isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToEntityListNull_shouldReturnEmpty() {
	
		List<Favoris> favlist = faconv.convertToEntityList(null);
		
		assertThat(favlist).isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToReadDTOListNull_shouldReturnEmpty() {
	
		List<FavorisReadDTO> favDTOlist = faconv.convertToDtoListReadOnly(null);
		
		assertThat(favDTOlist).isEmpty();
	}
	@SuppressWarnings("static-access")
	@Test
	void testConvToReadEntityListNull_shouldReturnEmpty() {
		
		List<Favoris> favlist = faconv.convertToEntityListReadOnly(null);
		
		assertThat(favlist).isEmpty();
	
	}
	
}

