# READ ME : 
# docker run -p 8080:8080 -d bsmehdi/mon-image-portfolio
# docker exec <container_id> java -jar Portfolio-0.0.1-SNAPSHOT.jar 

FROM mysql

RUN apt-get update
RUN mkdir -p /usr/share/man/man1
RUN apt-get install -y default-jre-headless

COPY ./target/Portfolio-0.0.1-SNAPSHOT.jar /

EXPOSE 8080
EXPOSE 3306

ENV MYSQL_ROOT_PASSWORD root

#RUN java -jar Portfolio-0.0.1-SNAPSHOT



